package com.dnprod.yonifkostrad;

import android.app.*;
import android.os.*;
import android.content.Intent;
import android.Manifest;

import me.pushy.sdk.Pushy;
import com.balsikandar.crashreporter.CrashReporter;
import java.io.File;

public class MainActivity extends BaseActivity 
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
		checkPermissions(new String[] {
				Manifest.permission.READ_EXTERNAL_STORAGE,
				Manifest.permission.WRITE_EXTERNAL_STORAGE,
				Manifest.permission.CAMERA,
				Manifest.permission.ACCESS_FINE_LOCATION,
				Manifest.permission.ACCESS_COARSE_LOCATION
			}, new PermissionListener() {

				@Override
				public void onPermissionGranted() {
					init();
				}

				@Override
				public void onPermissionDenied() {
				}
		});
    }
	
	public void init() {
		CrashReporter.initialize(this, new File(Environment.getExternalStorageDirectory(), "log.txt").getAbsolutePath());
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			int importance = NotificationManager.IMPORTANCE_DEFAULT;
			NotificationChannel channel = new NotificationChannel(Constants.NOTIFICATION_CHANNEL, ""+getAppName()+"'s notification channels", importance);
			channel.setDescription("Notification channel for "+getAppName());
			NotificationManager notificationManager = getSystemService(NotificationManager.class);
			notificationManager.createNotificationChannel(channel);
		}
		Pushy.listen(this);
		//writeEncrypted("email", "user1@gmail.com");
		//writeEncrypted("password", "HaloDunia123");
		/*new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

			@Override
			public void run() {
				System.exit(0);
			}
		}, 3*60*1000);*/
		Intent i = new Intent(this, SplashActivity.class);
		startActivity(i);
		finish();
	}
}
