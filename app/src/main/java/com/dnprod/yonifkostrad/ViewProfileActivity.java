package com.dnprod.yonifkostrad;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dnprod.yonifkostrad.adapter.PrivateMessageAdapter;
import android.location.Location;
import com.squareup.picasso.Picasso;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.graphics.Color;
import android.app.ProgressDialog;
import android.view.Menu;

public class ViewProfileActivity extends BaseActivity {
	ImageView profilePicture;
	int userID = 0;
	TextView nameView, distanceView, descriptionView;
	private final int SIGN_UP = 1;
	private final int TAKE_PHOTO = 2;
	private final int PICK_IMAGE_FROM_GALLERY = 3;
	LinearLayout loginContainer;
	RecyclerView messageList;
	ArrayList<JSONObject> messages;
	PrivateMessageAdapter adapter;
	EditText messageField;
	LinearLayoutManager lm;
	File photoFile;
	int start = 0;
	int length = 20;
	String profilePictureURL = "";
	FloatingActionButton favorite, favoriteCount;
	boolean isFavoriteUser = false;
	boolean blocked = false;
	Menu menu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_profile);
		setTitle("Lihat Profil");
		setSupportActionBar((Toolbar)findViewById(R.id.toolbar));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		profilePicture = findViewById(R.id.profile_picture);
		nameView = findViewById(R.id.name);
		distanceView = findViewById(R.id.distance);
		descriptionView = findViewById(R.id.description);
		favorite = findViewById(R.id.favorite);
		favoriteCount = findViewById(R.id.favorite_count);
		favoriteCount.setImageBitmap(Util.textToBitmap("0", 20, Color.WHITE));
		userID = getIntent().getIntExtra("user_id", 0);
		post(new Listener() {

				@Override
				public void onResponse(String response) {
					try {
						final JSONObject user = new JSONArray(response).getJSONObject(0);
						nameView.setText(getString(user, "name", "").trim());
						distanceView.setText("");
						descriptionView.setText(getString(user, "description", "").trim());
						profilePictureURL = Constants.IMAGE_URL+getString(user, "photo", "").trim();
						Picasso.get().load(Uri.parse(profilePictureURL)).resize(1024, 0).onlyScaleDown().into(profilePicture);
						profilePicture.setClickable(true);
						profilePicture.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View view) {
									Intent i = new Intent(ViewProfileActivity.this, ViewImageActivity.class);
									i.putExtra("img_url", profilePictureURL);
									startActivity(i);
								}
						});
						getLocation(new BaseActivity.LocationUpdateListener() {

								@Override
								public void onLocationUpdate(double lat, double lng) {
									double userLat = Util.getDouble(user, "lat", 0);
									double userLng = Util.getDouble(user, "lng", 0);
									float[] meters = {
										0, 0, 0, 0
									};
									Location.distanceBetween(lat, lng, userLat, userLng, meters);
									if (userLat == 0 && userLng == 0) {
										distanceView.setText("Lokasi tidak diketahui");
									} else {
										if (meters[0] < 1000) {
											distanceView.setText(""+meters[0]+" meter");
										} else {
											distanceView.setText(""+(meters[0]/1000)+" kilometer");
										}
										distanceView.setVisibility(View.VISIBLE);
									}
								}
						});
					} catch (Exception e) {}
				}
		}, Constants.PHP_URL+"/main/get_by_id", "name", "users", "id", ""+userID);
		post(new Listener() {

				@Override
				public void onResponse(String response) {
					try {
						JSONArray favorites = new JSONArray(response);
						int count = getInt(favorites.getJSONObject(0), "COUNT(*)", 0);
						favoriteCount.setImageBitmap(Util.textToBitmap(""+count, 20, Color.WHITE));
					} catch (Exception e) {}
				}
		}, Constants.PHP_URL+"/main/query", "cmd", "SELECT COUNT(*) FROM `favorite_users` WHERE `user_id`="+userID);
		loginContainer = findViewById(R.id.login_container);
		messageList = findViewById(R.id.messages);
		messageField = findViewById(R.id.message);
		if (Constants.USER_ID == 0) {
			loginContainer.setVisibility(View.VISIBLE);
		} else {
			loginContainer.setVisibility(View.GONE);
		}
		lm = new LinearLayoutManager(this);
		messageList.setLayoutManager(lm);
		messageList.setItemAnimator(new DefaultItemAnimator());
		messages = new ArrayList<>();
		adapter = new PrivateMessageAdapter(this, messages);
		messageList.setAdapter(adapter);
		messageList.addOnScrollListener(new RecyclerView.OnScrollListener() {

				@Override
				public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
					super.onScrolled(recyclerView, dx, dy);
					int pastVisibleItems = lm.findFirstCompletelyVisibleItemPosition();
					if (pastVisibleItems == 0) {
						getMessages(false);
					}
				}
			});
		final ProgressDialog dialog = createDialog("Memuat...");
		dialog.show();
		post(new Listener() {

				@Override
				public void onResponse(String response) {
					try {
						int length = new JSONArray(response).length();
						if (length > 0) {
							isFavoriteUser = true;
							favorite.setImageResource(R.drawable.unfavorite);
						}
						post(new Listener() {

								@Override
								public void onResponse(String response) {
									try {
										int length = new JSONArray(response).length();
										if (length > 0) {
											blocked = true;
											if (menu != null) {
												menu.findItem(R.id.block).setTitle("Buka Blokir");
												menu.findItem(R.id.block).setIcon(R.drawable.unblock);
											}
										}
										dialog.dismiss();
									} catch (Exception e) {}
								}
							}, Constants.PHP_URL+"/main/query", "cmd", "SELECT * FROM `blocked_users` WHERE `blocked_user_id`="+userID+" AND `user_id`="+Constants.USER_ID);
					} catch (Exception e) {}
				}
		}, Constants.PHP_URL+"/main/query", "cmd", "SELECT * FROM `favorite_users` WHERE `user_id`="+userID+" AND `favorite`="+Constants.USER_ID);
		start = 0;
		getMessages(true);
	}
	
	public void favorite(View view) {
		isFavoriteUser = !isFavoriteUser;
		if (isFavoriteUser) {
			favorite.setImageResource(R.drawable.unfavorite);
			post(new Listener() {

					@Override
					public void onResponse(String response) {
					}
			}, Constants.PHP_URL+"/main/execute", "cmd", "INSERT INTO `favorite_users` (`user_id`, `favorite`) VALUES ("+userID+", "+Constants.USER_ID+")");
		} else {
			favorite.setImageResource(R.drawable.favorite);
			post(new Listener() {

					@Override
					public void onResponse(String response) {
					}
				}, Constants.PHP_URL+"/main/execute", "cmd", "DELETE FROM `favorite_users` WHERE `user_id`="+userID+" AND `favorite`="+Constants.USER_ID);
		}
	}
	
	public void getMessages(final boolean needScroll) {
		post(new Listener() {

				@Override
				public void onResponse(String response) {
					try {
						JSONArray messagesJSON = new JSONArray(response);
						for (int i=0; i<messagesJSON.length(); i++) {
							messages.add(0, messagesJSON.getJSONObject(i));
						}
						adapter.notifyDataSetChanged();
						if (needScroll) {
							messageList.scrollToPosition(messages.size()-1);
						}
						start += length;
					} catch (Exception e) {}
				}
			}, Constants.PHP_URL+"/user/get_private_messages", "my_user_id", ""+Constants.USER_ID, "opponent_user_id", ""+userID, "start", ""+start, "length", ""+length);
	}

	public void sendMessage(View view) {
		if (blocked) {
			show("Tidak bisa mengirim pesan ke pengguna yang diblokir");
			return;
		}
		final String message = messageField.getText().toString().trim();
		if (message.equals("")) {
			show("Mohon masukkan isi pesan");
			return;
		}
		messageField.setText("");
		hideKeyboard();
		post(new Listener() {

				@Override
				public void onResponse(String response) {
					try {
						JSONObject obj = new JSONObject(response);
						messages.add(obj);
						adapter.notifyDataSetChanged();
						messageList.scrollToPosition(messages.size()-1);
					} catch (Exception e) {}
				}
			}, Constants.PHP_URL+"/user/send_message_to_user", "message", message, "sender_id", ""+Constants.USER_ID, "receiver_id", ""+userID, "date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	}
	
	public void chat(View view) {
		Intent i = new Intent(this, PrivateMessageActivity.class);
		i.putExtra("user_id", userID);
		startActivity(i);
	}

	public void login(View view) {
		startActivityForResult(new Intent(this, SignupActivity.class), SIGN_UP);
	}

	public void attach(View view) {
		if (blocked) {
			show("Tidak bisa mengirim pesan ke pengguna yang diblokir");
			return;
		}
		AlertDialog dialog = new AlertDialog.Builder(this)
			.setTitle("Ambil foto dari...")
			.setItems(new String[] {
				"Kamera", "Galeri"
			}, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface p1, int position) {
					if (position == 0) {
						Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
							photoFile = null;
							try {
								photoFile = createImageFile();
							} catch (Exception e) {
								e.printStackTrace();
							}
							if (photoFile != null) {
								Uri photoURI = FileProvider.getUriForFile(ViewProfileActivity.this, getPackageName()+".fileprovider", photoFile);
								takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
								startActivityForResult(takePictureIntent, TAKE_PHOTO);
							}
						}
					} else if (position == 1) {
						Intent intent = new Intent();
						intent.setType("image/*");
						intent.setAction(Intent.ACTION_GET_CONTENT);
						startActivityForResult(Intent.createChooser(intent, "Pilih gambar..."), PICK_IMAGE_FROM_GALLERY);
					}
				}
			})
			.create();
		dialog.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.menu = menu;
		getMenuInflater().inflate(R.menu.view_profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
		} else if (id == R.id.block) {
			if (blocked) {
				AlertDialog dialog = new AlertDialog.Builder(this)
					.setMessage("Apakah Anda yakin ingin membuka blokir pengguna ini?")
					.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface p1, int p2) {
							final ProgressDialog dialog = createDialog("Membuka blokir pengguna...");
							dialog.show();
							post(new Listener() {

									@Override
									public void onResponse(String response) {
										blocked = false;
										dialog.dismiss();
									}
								}, Constants.PHP_URL+"/main/execute", "cmd", "DELETE FROM `blocked_users` WHERE `blocked_user_id`="+userID+" AND `user_id`="+Constants.USER_ID);
						}
					})
					.setNegativeButton("Tidak", null)
					.create();
				dialog.show();
			} else {
				AlertDialog dialog = new AlertDialog.Builder(this)
					.setMessage("Apakah Anda yakin ingin memblokir pengguna ini?")
					.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface p1, int p2) {
							final ProgressDialog dialog = createDialog("Memblokir pengguna...");
							dialog.show();
							post(new Listener() {

									@Override
									public void onResponse(String response) {
										blocked = true;
										dialog.dismiss();
									}
								}, Constants.PHP_URL+"/main/execute", "cmd", "INSERT INTO `blocked_users` (`blocked_user_id`, `user_id`) VALUES ("+userID+", "+Constants.USER_ID+")");
						}
					})
					.setNegativeButton("Tidak", null)
					.create();
				dialog.show();
			}
		}
		return false;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == SIGN_UP) {
				loginContainer.setVisibility(View.GONE);
			} else if (requestCode == TAKE_PHOTO) {
				uploadPhoto();
			} else if (requestCode == PICK_IMAGE_FROM_GALLERY) {
				try {
					photoFile = new File(getFilesDir(), UUID.randomUUID().toString()+".png");
					InputStream stream = getContentResolver().openInputStream(data.getData());
					FileOutputStream fos = new FileOutputStream(photoFile);
					int read;
					byte[] buffer = new byte[8192];
					while ((read = stream.read(buffer)) != -1) {
						fos.write(buffer, 0, read);
					}
					fos.flush();
					fos.close();
					stream.close();
				} catch (Exception e) {}
				uploadPhoto();
			}
		}
	}

	public void uploadPhoto() {
		post(new Listener() {

				@Override
				public void onResponse(String response) {
					try {
						JSONObject obj = new JSONObject(response);
						messages.add(obj);
						adapter.notifyDataSetChanged();
						messageList.scrollToPosition(messages.size()-1);
					} catch (Exception e) {}
				}
			}, Constants.PHP_URL+"/user/send_image_to_user", "sender_id", ""+Constants.USER_ID, "receiver_id", ""+userID, "file", UUID.randomUUID().toString()+".jpg", "image/jpeg", photoFile.getAbsolutePath(), "date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	}
}
