package com.dnprod.yonifkostrad;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.view.View;
import android.graphics.Bitmap;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

public class ViewDocumentActivity extends BaseActivity {
	ProgressBar progress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_document);
		setTitle(getIntent().getStringExtra("title"));
		setSupportActionBar((Toolbar)findViewById(R.id.toolbar));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		WebView urlWebView = findViewById(R.id.web);
		progress = findViewById(R.id.progress);
		urlWebView.setWebViewClient(new AppWebViewClients());
		urlWebView.getSettings().setJavaScriptEnabled(true);
		urlWebView.getSettings().setUseWideViewPort(true);
		urlWebView.loadUrl(getIntent().getStringExtra("url"));
	}
	
	public class AppWebViewClients extends WebViewClient {
		
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url)
		{
			// TODO Auto-generated method stub
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon)
		{
			// TODO: Implement this method
			super.onPageStarted(view, url, favicon);
			progress.setProgress(10);
			progress.setVisibility(View.VISIBLE);
		}

		@Override
		public void onPageFinished(WebView view, String url)
		{
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);
			progress.setVisibility(View.GONE);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// TODO: Implement this method
		if (item.getItemId() == android.R.id.home) {
			finish();
		}
		return false;
	}
}
