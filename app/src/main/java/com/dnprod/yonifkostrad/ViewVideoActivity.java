package com.dnprod.yonifkostrad;

import android.os.Bundle;
import android.widget.VideoView;
import android.widget.MediaController;

public class ViewVideoActivity extends BaseActivity
{
	VideoView video;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO: Implement this method
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_video);
		setTitle("");
		video = findViewById(R.id.video);
		MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(video);
        video.setMediaController(mediaController);
        log("Video URL: "+getIntent().getStringExtra("url").replace("https://", "http://"));
		video.setVideoPath(getIntent().getStringExtra("url").replace("https://", "http://"));
		video.start();
	}
}
