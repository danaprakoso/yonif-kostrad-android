package com.dnprod.yonifkostrad;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import org.json.JSONObject;

public class Util {

	public static void show(final Context ctx, final String message) {
		runLater(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
				}
			});
	}

	public static void log(String message) {
		//Log.e(Build.MODEL, message);
	}

	public static String getString(JSONObject obj, String name, String defaultValue) {
		try {
			String value = obj.getString(name);
			if (value == null || value.equals("null") || value.equals("NULL")) {
				return defaultValue;
			} else {
				return value;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return defaultValue;
	}

	public static int getInt(JSONObject obj, String name, int defaultValue) {
		try {
			String value = obj.getString(name);
			if (value == null || value.equals("null") || value.equals("NULL")) {
				return defaultValue;
			} else {
				return Integer.parseInt(value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return defaultValue;
	}
	
	public static double getDouble(JSONObject obj, String name, double defaultValue) {
		try {
			String value = obj.getString(name);
			if (value == null || value.equals("null") || value.equals("NULL")) {
				return defaultValue;
			} else {
				return Double.parseDouble(value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return defaultValue;
	}

	public static String read(Context ctx, String name, String defaultValue) {
		SharedPreferences sp = ctx.getSharedPreferences("data", Context.MODE_PRIVATE);
		return sp.getString(name, defaultValue);
	}

	public static String readEncrypted(Context ctx, String name, String defaultValue) {
		SharedPreferences sp = ctx.getSharedPreferences("data", Context.MODE_PRIVATE);
		String value = sp.getString(encrypt(name), "");
		if (value == null || value.trim().equals("")) {
			return defaultValue;
		}
		try {
			return new String(Base64.decode(value, Base64.DEFAULT));
		} catch (Exception e) {}
		return defaultValue;
	}

	public static int read(Context ctx, String name, int defaultValue) {
		SharedPreferences sp = ctx.getSharedPreferences("data", Context.MODE_PRIVATE);
		return sp.getInt(name, defaultValue);
	}

	public static int readEncrypted(Context ctx, String name, int defaultValue) {
		SharedPreferences sp = ctx.getSharedPreferences("data", Context.MODE_PRIVATE);
		String value = sp.getString(encrypt(name), "");
		if (value == null || value.trim().equals("")) {
			return defaultValue;
		}
		try {
			return Integer.parseInt(new String(Base64.decode(value, Base64.DEFAULT)));
		} catch (Exception e) {}
		return defaultValue;
	}

	public static void write(Context ctx, String name, int value) {
		SharedPreferences sp = ctx.getSharedPreferences("data", Context.MODE_PRIVATE);
		SharedPreferences.Editor e = sp.edit();
		e.putInt(name, value);
		e.commit();
	}

	public static void writeEncrypted(Context ctx, String name, String value) {
		SharedPreferences sp = ctx.getSharedPreferences("data", Context.MODE_PRIVATE);
		SharedPreferences.Editor e = sp.edit();
		try
		{
			e.putString(encrypt(name), encrypt(value));
		}
		catch (Exception exception)
		{}
		e.commit();
	}
	
	public static String encrypt(String value) {
		try {
			return Base64.encodeToString(value.getBytes(), Base64.DEFAULT);
		} catch (Exception e) {}
		return "";
	}

	public static void write(Context ctx, String name, String value) {
		SharedPreferences sp = ctx.getSharedPreferences("data", Context.MODE_PRIVATE);
		SharedPreferences.Editor e = sp.edit();
		e.putString(name, value);
		e.commit();
	}

	public static void writeEncrypted(Context ctx, String name, int value) {
		SharedPreferences sp = ctx.getSharedPreferences("data", Context.MODE_PRIVATE);
		SharedPreferences.Editor e = sp.edit();
		e.putString(encrypt(name), Base64.encodeToString((""+value).getBytes(), Base64.DEFAULT));
		e.commit();
	}

	public static void run(Runnable runnable)
	{
		new Thread(runnable).start();
	}

	public static void runLater(Runnable runnable)
	{
		new Handler(Looper.getMainLooper()).post(runnable);
	}
	
	public static OkHttpClient getOkHttpClient() {
		OkHttpClient client = new OkHttpClient();
		client.setReadTimeout(30, TimeUnit.SECONDS);
		client.setWriteTimeout(30, TimeUnit.SECONDS);
		client.setConnectTimeout(30, TimeUnit.SECONDS);
		client.setRetryOnConnectionFailure(true);
		/*client.interceptors().add(new Interceptor() {

			@Override
			public Response intercept(Chain chain) throws IOException {
				Request request   = chain.request();
				Response response = chain.proceed(request);
				ResponseBody body = response.body();
				// Only intercept JSON type responses and ignore the rest.
				if (body != null && body.contentType() != null && body.contentType().subtype() != null && body.contentType().subtype().toLowerCase().equals("json")) {
					String errorMessage = "";
					int errorCode = 200; // Assume default OK
					try {
						BufferedSource source = body.source();
						source.request(Long.MAX_VALUE); // Buffer the entire body.
						Buffer buffer   = source.buffer();
						Charset charset = body.contentType().charset(Charset.forName("UTF-8"));
						// Clone the existing buffer is they can only read once so we still want to pass the original one to the chain.
						String json = buffer.clone().readString(charset);
						JSONObject obj = new JSONObject(json);
						// Capture error code an message.
						if (obj.has("status")) {
							errorCode = obj.getInt("status");
						}
						if (obj.has("message")) {
							errorMessage = obj.getString("message");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					// Check if status has an error code then throw and exception so retrofit can trigger the onFailure callback method.
					// Anything above 400 is treated as a server error.
					if (errorCode > 399) {
						throw new IOException("Server error code: " + errorCode + " with error message: " + errorMessage);
					}
				}
				return response;
			}
		});*/
		//client.setProtocols(Arrays.asList(Protocol.HTTP_1_1));
		/*try {
			final TrustManager[] trustAllCerts = new TrustManager[] {
				new X509TrustManager() {
					@Override
					public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
					}

					@Override
					public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
					}

					@Override
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return new java.security.cert.X509Certificate[]{};
					}
				}
			};
			// Install the all-trusting trust manager
			final SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
			// Create an ssl socket factory with our all-trusting manager
			final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
			client.setSslSocketFactory(sslSocketFactory);
			client.setHostnameVerifier(new HostnameVerifier() {

					@Override
					public boolean verify(String hostname, SSLSession session) {
						return true;
					}
				});
		} catch (Exception e) {}*/
		return client;
	}
	
	/*public static void get(final Context ctx, final Listener listener, final String url) {
		run(new Runnable() {

				public void run() {
					try {
						URLConnection c = new URL(url).openConnection();
						c.setDoInput(true);
						c.setDoOutput(false);
						c.connect();
						InputStream stream = c.getInputStream();
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						int read;
						byte[] buffer = new byte[8192];
						while ((read = stream.read(buffer)) != -1) {
							baos.write(buffer, 0, read);
						}
						baos.flush();
						final String response = baos.toString();
						baos.close();
						stream.close();
						runLater(new Runnable() {

								public void run() {
									//log("Response: "+response);
									if (listener != null) {
										listener.onResponse(response);
									}
								}
							});
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
	}*/

	private static int CURRENT_GET_RETRY = 0;

	public static void get(final Context ctx, final Listener listener, final String url) {
		CURRENT_GET_RETRY = 0;
		_get(ctx, listener, url);
	}

	public static void _get(final Context ctx, final Listener listener, final String url) {
		if (CURRENT_GET_RETRY < 5) {
			return;
		}
		run(new Runnable() {

				public void run() {
					try {
						OkHttpClient client = getOkHttpClient();
						Request request = new Request.Builder()
							.url(url)
							.build();
						final String response = client.newCall(request).execute().body().string();
						runLater(new Runnable() {

								public void run() {
									//log("Response: "+response);
									if (listener != null) {
										listener.onResponse(response);
									}
								}
							});
					} catch (Exception e) {
						e.printStackTrace();
						CURRENT_GET_RETRY++;
						get(ctx, listener, url);
					}
				}
			});
	}
	
	/*public static void post(final Context ctx, final Listener listener, final String url, final String ...params) {
		run(new Runnable() {

				public void run() {
					try {
						MultipartUtility utility = new MultipartUtility(url, "UTF-8", "==="+System.currentTimeMillis()+"===");
						for (int i=0; i<params.length;) {
							if (params[i].equals("file")) {
								utility.addFilePart("file", new File(params[i+3]));
								i += 4;
							} else {
								utility.addFormField(params[i], params[i+1]);
								i += 2;
							}
						}
						final String response = utility.execute();
						runLater(new Runnable() {

								public void run() {
									//log("Response: "+response);
									if (listener != null) {
										listener.onResponse(response);
									}
								}
							});
					} catch (final Exception e) {
						e.printStackTrace();
						runLater(new Runnable() {

								@Override
								public void run()
								{
									// TODO: Implement this method
									show(ctx, e.getMessage());
								}
							});
					}
				}
			});
	}*/

	private static int CURRENT_POST_RETRY = 0;

	public static void post(final Context ctx, final Listener listener, final String url, final String ...params) {
		CURRENT_POST_RETRY = 0;
		_post(ctx, listener, url, params);
	}

	public static void _post(final Context ctx, final Listener listener, final String url, final String ...params) {
		if (CURRENT_POST_RETRY > 5) {
			return;
		}
		run(new Runnable() {

				public void run() {
					try {
						OkHttpClient client = getOkHttpClient();
						MultipartBuilder builder = new MultipartBuilder()
							.type(MultipartBuilder.FORM);
						for (int i=0; i<params.length;) {
							if (params[i].equals("file")) {
								String fileName = params[i+1];
								String mimeType = params[i+2];
								String filePath = params[i+3];
								builder.addFormDataPart("file", fileName,
														RequestBody.create(MediaType.parse(mimeType), new File(filePath)));
								i += 4;
							} else {
								builder.addFormDataPart(params[i], params[i+1]);
								i += 2;
							}
						}
						Request request = new Request.Builder()
							.url(url)
							.post(builder.build())
							.build();
						final String response = client.newCall(request).execute().body().string();
						runLater(new Runnable() {

								public void run() {
									if (listener != null) {
										listener.onResponse(response);
									}
								}
							});
					} catch (final Exception e) {
						e.printStackTrace();
						CURRENT_POST_RETRY++;
						_post(ctx, listener, url, params);
						runLater(new Runnable() {

								@Override
								public void run()
								{
									// TODO: Implement this method
									//show(ctx, e.getMessage());
								}
							});
					}
				}
			});
	}
	
	public static InputStream trustedCertificatesInputStream(Context ctx) throws IOException {
		return ctx.getAssets().open("siapsiaga.cer");
    }
	
    public static X509TrustManager trustManagerForCertificates(InputStream in)
	throws GeneralSecurityException {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        Collection<? extends Certificate> certificates = certificateFactory.generateCertificates(in);
        if (certificates.isEmpty()) {
            throw new IllegalArgumentException("expected non-empty set of trusted certificates");
        }

        // Put the certificates a key store.
        char[] password = "password".toCharArray(); // Any password will work.
        KeyStore keyStore = newEmptyKeyStore(password);
        int index = 0;
        for (Certificate certificate : certificates) {
            String certificateAlias = Integer.toString(index++);
            keyStore.setCertificateEntry(certificateAlias, certificate);
        }

        // Use it to build an X509 trust manager.
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(
			KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(keyStore, password);
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
			TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(keyStore);
        TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
        if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
            throw new IllegalStateException("Unexpected default trust managers:"
											+ Arrays.toString(trustManagers));
        }
        return (X509TrustManager) trustManagers[0];
    }

    public static KeyStore newEmptyKeyStore(char[] password) throws GeneralSecurityException {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream in = null; // By convention, 'null' creates an empty key store.
            keyStore.load(in, password);
            return keyStore;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

	public interface Listener
	{

		void onResponse(String response);
	}

	public static ProgressDialog createDialog(Context ctx, String message) {
		ProgressDialog dialog = new ProgressDialog(ctx);
		dialog.setCancelable(false);
		dialog.setMessage(message);
		return dialog;
	}

	public static ProgressDialog createDialog(Context ctx, int messageID) {
		ProgressDialog dialog = new ProgressDialog(ctx);
		dialog.setCancelable(false);
		dialog.setMessage(ctx.getResources().getString(messageID));
		return dialog;
	}

	public static void dismissDialog(final ProgressDialog dialog) {
		runLater(new Runnable() {

				@Override
				public void run() {
					if (dialog.isShowing()) {
						dialog.dismiss();
					}
				}
			});
	}

	public static String formatPhoneNumber(String number) {
		number = number.replaceAll("[^0-9]+", "");
		return number;
	}

	public static String formatPrice(String price) {
		price = price.replaceAll("[^0-9]+", "");
		return "Rp "+new DecimalFormat("#,###").format(Integer.parseInt(price))+",00";
	}

	public static String removeNonDigitValues(String price) {
		return price.replaceAll("[^0-9]+", "");
	}
	
	public static boolean isPackageInstalled(PackageManager packageManager, String packageName) {
		try {
			packageManager.getPackageInfo(packageName, 0);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}
	
	public static int getRadioButtonIndex(RadioGroup checkBoxGroup) {
		return checkBoxGroup.indexOfChild(checkBoxGroup.findViewById(checkBoxGroup.getCheckedRadioButtonId()));
	}
	
	public static File createImageFile(Context ctx) {
		try {
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
			String imageFileName = "JPEG_" + timeStamp + "_";
			File storageDir = ctx.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
			File image = File.createTempFile(
				imageFileName,  /* prefix */
				".jpg",         /* suffix */
				storageDir      /* directory */
			);
			return image;
		} catch (Exception e) {}
		return null;
	}
	
	public static Bitmap getCroppedBitmap(Bitmap bitmap) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);
		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		// canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
		canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getWidth()/2, bitmap.getWidth() / 2, paint);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		//Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
		//return _bmp;
		return output;
	}
	
	public static Bitmap textToBitmap(String text, float textSize, int textColor) {
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setTextSize(textSize);
		paint.setColor(textColor);
		paint.setTextAlign(Paint.Align.LEFT);
		float baseline = -paint.ascent(); // ascent() is negative
		int width = (int) (paint.measureText(text) + 0.0f); // round
		int height = (int) (baseline + paint.descent() + 0.0f);
		Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(image);
		canvas.drawText(text, 0, baseline, paint);
		return image;
	}
	
	public static void showLog(Context ctx, String message) {
		Intent sendIntent = new Intent(Intent.ACTION_VIEW);         
		sendIntent.setData(Uri.parse("sms:"));
		sendIntent.putExtra("sms_body", message);
		ctx.startActivity(sendIntent);
	}
	
	public static String getExtension(String path) {
		return path.substring(path.lastIndexOf("."), path.length());
	}
	
	public static String getMediaType(Context ctx, Uri uri) {
		return ctx.getContentResolver().getType(uri);
	}
}
