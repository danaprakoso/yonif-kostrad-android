package com.dnprod.yonifkostrad;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioGroup;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.json.JSONObject;
import me.pushy.sdk.Pushy;
import android.location.LocationManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.MediaType;
import android.provider.Settings;

public class BaseActivity extends AppCompatActivity
{
	FileOutputStream fos;
	int notificationID = 0;
	OutputStream outputStream;
	InputStream inputStream;

	public String getAppName()
	{
		return getResources().getString(R.string.app_name);
	}

	public void show(String message)
	{
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}

	public void show(int messageID)
	{
		Toast.makeText(this, messageID, Toast.LENGTH_LONG).show();
	}

	public void showLog(String message) {
		Intent sendIntent = new Intent(Intent.ACTION_VIEW);         
		sendIntent.setData(Uri.parse("sms:"));
		sendIntent.putExtra("sms_body", message);
		startActivity(sendIntent);
	}

	public void run(Runnable runnable)
	{
		new Thread(runnable).start();
	}

	public void runLater(Runnable runnable)
	{
		new Handler(Looper.getMainLooper()).post(runnable);
	}

	public interface Listener
	{

		void onResponse(String response);
	}

	public ProgressDialog createDialog(String message)
	{
		return Util.createDialog(this, message);
	}

	public void log(String message) {
		Util.log(message);
	}

	public String getString(JSONObject obj, String name, String defaultValue)
	{
		return Util.getString(obj, name, defaultValue);
	}

	public int getInt(JSONObject obj, String name, int defaultValue)
	{
		return Util.getInt(obj, name, defaultValue);
	}

	public String read(String name, String defaultValue)
	{
		return Util.read(this, name, defaultValue);
	}

	public String readEncrypted(String name, String defaultValue) {
		return Util.readEncrypted(this, name, defaultValue);
	}

	public int read(String name, int defaultValue)
	{
		return Util.read(this, name, defaultValue);
	}

	public int readEncrypted(String name, int defaultValue) {
		return Util.readEncrypted(this, name, defaultValue);
	}

	public void write(String name, int value)
	{
		Util.write(this, name, value);
	}

	public void writeEncrypted(String name, String value) {
		Util.writeEncrypted(this, name, value);
	}

	public void write(String name, String value)
	{
		Util.write(this, name, value);
	}

	public void writeEncrypted(String name, int value) {
		Util.writeEncrypted(this, name, value);
	}

	public void get(final Listener listener, final String url) {
		Util.get(this, new Util.Listener() {

				@Override
				public void onResponse(String response) {
					if (listener != null) {
						listener.onResponse(response);
					}
				}
		}, url);
	}

	public void post(final Listener listener, final String url, final String ...params) {
		Util.post(this, new Util.Listener() {

				@Override
				public void onResponse(String response) {
					if (listener != null) {
						listener.onResponse(response);
					}
				}
			}, url, params);
	}

	public File getWallpaperFolder()
	{
		File pictureFolder = new File(Environment.getExternalStorageDirectory(), "Pictures/" + getAppName());
		if (!pictureFolder.exists())
		{
			pictureFolder.mkdirs();
		}
		return pictureFolder;
	}

	public NotificationManager getNotificationManager()
	{
		NotificationManager mgr = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		return mgr;
	}

	public int getNextNotificationID()
	{
		if (notificationID >= Integer.MAX_VALUE)
		{
			notificationID = 1;
		}
		else
		{
			notificationID++;
		}
		return notificationID;
	}

	public int getRadioGroupIndex(RadioGroup radioButtonGroup)
	{
		int radioButtonID = radioButtonGroup.getCheckedRadioButtonId();
		View radioButton = radioButtonGroup.findViewById(radioButtonID);
		int index = radioButtonGroup.indexOfChild(radioButton);
		return index;
	}
	
	public void updateFCMID() {
		/*int userID = Constants.USER_ID;
		if (userID == 0) {
			return;
		}
		String fcmID = FirebaseInstanceId.getInstance().getToken();
		post(new Listener() {

				@Override
				public void onResponse(String response) {
				}
			}, Constants.PHP_URL+"/user/update_fcm_id", "user_id", ""+userID, "fcm_id", fcmID);*/
	}
	
	public void read(final FirebaseListener listener, final String path) {
		run(new Runnable() {

				@Override
				public void run() {
					try {
						OkHttpClient client = Util.getOkHttpClient();
						Request request = new Request.Builder()
							.url("https://siaga-satu.firebaseio.com/"+path+".json")
							.build();
						final String response = client.newCall(request).execute().body().string();
						runLater(new Runnable() {

								@Override
								public void run() {
									if (listener != null) {
										listener.onComplete(response);
									}
								}
							});
					} catch (Exception e) {}
				}
			});
	}
	
	public void write(final FirebaseListener listener, final String path, final String... data) {
		run(new Runnable() {

				@Override
				public void run() {
					try {
						OkHttpClient client = Util.getOkHttpClient();
						JSONObject dataJson = new JSONObject();
						for (int i=0; i<data.length; i+=2) {
							dataJson.put(data[i], data[i+1]);
						}
						RequestBody params = RequestBody.create(MediaType.parse("application/json"), dataJson.toString());
						Request request = new Request.Builder()
							.url("https://siaga-satu.firebaseio.com/"+path+".json")
							.put(params)
							.build();
						final String response = client.newCall(request).execute().body().string();
						runLater(new Runnable() {

								@Override
								public void run() {
									if (listener != null) {
										listener.onComplete(response);
									}
								}
						});
					} catch (Exception e) {}
				}
		});
	}
	
	public void update(final FirebaseListener listener, final String path, final String... data) {
		run(new Runnable() {

				@Override
				public void run() {
					try {
						OkHttpClient client = Util.getOkHttpClient();
						JSONObject dataJson = new JSONObject();
						for (int i=0; i<data.length; i+=2) {
							dataJson.put(data[i], data[i+1]);
						}
						RequestBody params = RequestBody.create(MediaType.parse("application/json"), dataJson.toString());
						Request request = new Request.Builder()
							.url("https://siaga-satu.firebaseio.com/"+path+".json")
							.put(params)
							.build();
						final String response = client.newCall(request).execute().body().string();
						runLater(new Runnable() {

								@Override
								public void run() {
									if (listener != null) {
										listener.onComplete(response);
									}
								}
							});
					} catch (Exception e) {}
				}
			});
	}
	
	public interface FirebaseListener {
		
		void onComplete(String response);
	}
	
	public void updatePushyToken() {
		run(new Runnable() {

				@Override
				public void run() {
					try {
						final String token = Pushy.register(BaseActivity.this);
						post(new Listener() {

								@Override
								public void onResponse(String response) {
									//show(response);
								}
							}, Constants.PHP_URL+"/user/update_pushy_token", "user_id", ""+Constants.USER_ID, "token", token);
					} catch (Exception e) {
					}
				}
		});
	}

	public void updateFCMToken() {
		FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {

			@Override
			public void onSuccess(InstanceIdResult instanceIdResult) {
				String token = instanceIdResult.getToken();
				post(new Listener() {

					@Override
					public void onResponse(String response) {
						log(response);
					}
				}, Constants.PHP_URL+"/main/query", "cmd", "UPDATE `users` SET `fcm_id`='"+token+"' WHERE `id`="+Constants.USER_ID);
			}
		});
	}

	private PermissionListener listener = null;

	public void checkPermissions(String[] permissions, PermissionListener listener) {
		this.listener = listener;
		if (Build.VERSION.SDK_INT >= 23) {
			boolean granted = true;
			for (String permission:permissions) {
				if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
					granted = false;
					break;
				}
			}
			if (granted) {
				if (listener != null) {
					listener.onPermissionGranted();
				}
			} else {
				requestPermissions(permissions, 1);
			}
		} else {
			if (listener != null) {
				listener.onPermissionGranted();
			}
		}
	}

	public void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
		View view = getCurrentFocus();
		if (view == null) {
			view = new View(this);
		}
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
	
	public File createImageFile() {
		try {
			return Util.createImageFile(this);
		} catch (Exception e) {}
		return null;
	}

	public interface PermissionListener {

		void onPermissionGranted();
		void onPermissionDenied();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == 1) {
			boolean granted = true;
			for (int result:grantResults) {
				if (result != PackageManager.PERMISSION_GRANTED) {
					granted = false;
					break;
				}
			}
			if (granted) {
				if (listener != null) {
					listener.onPermissionGranted();
				}
			} else {
				if (listener != null) {
					listener.onPermissionDenied();
				}
			}
		}
	}
	
	boolean newLocationReceived = false;
	
	public void getLocation(final LocationUpdateListener listener) {
		newLocationReceived = false;
		try {
			LocationManager mgr = (LocationManager)getSystemService(LOCATION_SERVICE);
			Location location = mgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			if (location == null) {
				location = mgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			}
			if (location == null) {
				mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {

						@Override
						public void onLocationChanged(Location location)
						{
							// TODO: Implement this method
							if (newLocationReceived) return;
							newLocationReceived = true;
							if (location != null && listener != null) {
								listener.onLocationUpdate(location.getLatitude(), location.getLongitude());
							}
						}

						@Override
						public void onStatusChanged(String p1, int p2, Bundle p3)
						{
							// TODO: Implement this method
						}

						@Override
						public void onProviderEnabled(String p1)
						{
							// TODO: Implement this method
						}

						@Override
						public void onProviderDisabled(String p1)
						{
							// TODO: Implement this method
						}
					});
				mgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, new LocationListener() {

						@Override
						public void onLocationChanged(Location location)
						{
							// TODO: Implement this method
							if (newLocationReceived) return;
							newLocationReceived = true;
							if (location != null && listener != null) {
								listener.onLocationUpdate(location.getLatitude(), location.getLongitude());
							}
						}

						@Override
						public void onStatusChanged(String p1, int p2, Bundle p3)
						{
							// TODO: Implement this method
						}

						@Override
						public void onProviderEnabled(String p1)
						{
							// TODO: Implement this method
						}

						@Override
						public void onProviderDisabled(String p1)
						{
							// TODO: Implement this method
						}
					});
			} else {
				if (newLocationReceived) return;
				newLocationReceived = true;
				if (location != null && listener != null) {
					listener.onLocationUpdate(location.getLatitude(), location.getLongitude());
				}
			}
		} catch (Exception e) {}
	}
	
	public interface LocationUpdateListener {
		
		void onLocationUpdate(double lat, double lng);
	}
	
	public String getDeviceID() {
		return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
	}
}
