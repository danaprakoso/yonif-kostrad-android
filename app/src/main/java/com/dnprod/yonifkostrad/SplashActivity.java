package com.dnprod.yonifkostrad;

import android.os.Bundle;
import android.widget.ProgressBar;
import android.view.View;
import android.os.Handler;
import android.os.Looper;
import android.content.Intent;

import androidx.annotation.NonNull;

import android.app.ProgressDialog;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONObject;

public class SplashActivity extends BaseActivity {
	private final int SIGN_IN = 1;
	ProgressBar progress;
	SignInButton signIn;
	GoogleSignInClient client;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		progress = findViewById(R.id.progress);
		signIn = findViewById(R.id.sign_in);
		progress.setVisibility(View.GONE);
		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestIdToken(getString(R.string.web_client_id))
				.requestEmail()
				.build();
		client = GoogleSignIn.getClient(this, gso);
		new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

				@Override
				public void run() {
					progress.setVisibility(View.VISIBLE);
					new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

							@Override
							public void run() {
								progress.setVisibility(View.GONE);
								signIn.setVisibility(View.VISIBLE);
								signIn.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										Intent signInIntent = client.getSignInIntent();
										startActivityForResult(signInIntent, SIGN_IN);
									}
								});
							}
						}, 2000);
				}
		}, 1000);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == SIGN_IN) {
			try {
				Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
				if (task != null) {
					final GoogleSignInAccount account = task.getResult(ApiException.class);
					if (account != null) {
						final ProgressDialog dialog = createDialog("Memuat...");
						dialog.show();
						AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
						final FirebaseAuth auth = FirebaseAuth.getInstance();
						auth.signInWithCredential(credential)
								.addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
									@Override
									public void onComplete(@NonNull Task<AuthResult> task) {
										if (task.isSuccessful()) {
											FirebaseUser user = auth.getCurrentUser();
											if (user != null) {
												Constants.EMAIL = account.getEmail();
												post(new Listener() {

													@Override
													public void onResponse(String response) {
														try {
															JSONObject obj = new JSONObject(response);
															int responseCode = Util.getInt(obj, "response_code", 0);
															if (responseCode == 1) {
																Constants.USER_ID = Util.getInt(obj, "user_id", 0);
																Constants.ROLE = Util.getInt(obj, "role", 0);
																/*getLocation(new LocationUpdateListener() {
																	@Override
																	public void onLocationUpdate(double lat, double lng) {
																		final List<LatLng> areas1 = new ArrayList<>();
																		areas1.add(new LatLng(-7.408248,111.4172173));
																		areas1.add(new LatLng(-7.407908,111.4267763));
																		areas1.add(new LatLng(-7.418526,111.4268943));
																		areas1.add(new LatLng(-7.418952,111.4132903));
																		final List<LatLng> areas2 = new ArrayList<>();
																		areas2.add(new LatLng(-7.403472,111.4240023));
																		areas2.add(new LatLng(-7.403557,111.4309333));
																		areas2.add(new LatLng(-7.403738,111.4366737));
																		areas2.add(new LatLng(-7.410536,111.4199443));
																		if (PolyUtil.containsLocation(new LatLng(lat, lng), areas1, true)) {
																			dialog.dismiss();
																			startActivity(new Intent(SplashActivity.this, HomeActivity.class));
																			finish();
																		} else {
																			if (PolyUtil.containsLocation(new LatLng(lat, lng), areas2, true)) {
																				dialog.dismiss();
																				startActivity(new Intent(SplashActivity.this, HomeActivity.class));
																				finish();
																			} else {
																				dialog.dismiss();
																				show("Maaf, Anda tidak memiliki akses ke aplikasi ini.");
																			}
																		}
																	}
																});*/
																startActivity(new Intent(SplashActivity.this, HomeActivity.class));
																finish();
															} else if (responseCode == 0) {
																FirebaseAuth.getInstance().signOut();
																dialog.dismiss();
																show("Maaf, Anda tidak memiliki akses ke aplikasi ini.");
																client.signOut();
															}
														} catch (Exception e) {
															e.printStackTrace();
														}
													}
												}, Constants.PHP_URL+"/main/check_email", "email", account.getEmail());
											}
										}
									}
								});
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
