package com.dnprod.yonifkostrad;

import android.os.Bundle;
import android.widget.EditText;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.app.ProgressDialog;

public class AddContactActivity extends BaseActivity
{
	EditText nameField, phoneField;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO: Implement this method
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_contact);
		setTitle("Tambah Kontak");
		setSupportActionBar((Toolbar)findViewById(R.id.toolbar));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		nameField = findViewById(R.id.name);
		phoneField = findViewById(R.id.phone);
	}
	
	public void add(View view) {
		final String name = nameField.getText().toString().trim();
		final String phone = phoneField.getText().toString().trim();
		if (name.equals("")) {
			show("Mohon masukkan nama");
			return;
		}
		if (phone.equals("")) {
			show("Mohon masukkan nomor HP");
			return;
		}
		final ProgressDialog dialog = createDialog("Menambah kontak...");
		dialog.show();
		post(new Listener() {

				@Override
				public void onResponse(String response) {
					dialog.dismiss();
					setResult(RESULT_OK);
					finish();
				}
		}, Constants.PHP_URL+"/main/execute", "cmd", "INSERT INTO `contacts` (`name`, `phone`) VALUES ('"+name+"', '"+phone+"')");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// TODO: Implement this method
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
		}
		return false;
	}
}
