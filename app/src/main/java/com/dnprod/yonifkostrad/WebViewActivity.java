package com.dnprod.yonifkostrad;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.view.View;
import android.graphics.Bitmap;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

public class WebViewActivity extends BaseActivity {
	ProgressBar progress;
	WebView urlWebView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_view);
		setTitle(getIntent().getStringExtra("title"));
		setSupportActionBar((Toolbar)findViewById(R.id.toolbar));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		urlWebView = findViewById(R.id.web);
		progress = findViewById(R.id.progress);
		urlWebView.setWebViewClient(new AppWebViewClients());
		urlWebView.getSettings().setJavaScriptEnabled(true);
		urlWebView.getSettings().setSupportZoom(true);
		urlWebView.getSettings().setDisplayZoomControls(true);
		urlWebView.getSettings().setBuiltInZoomControls(true);
		urlWebView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 7.0; SM-G930V Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.125 Mobile Safari/537.36");
		urlWebView.loadUrl(getIntent().getStringExtra("url"));
	}

	public class AppWebViewClients extends WebViewClient {

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon)
		{
			// TODO: Implement this method
			super.onPageStarted(view, url, favicon);
			progress.setProgress(10);
			progress.setVisibility(View.VISIBLE);
		}

		@Override
		public void onPageFinished(WebView view, String url)
		{
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);
			progress.setVisibility(View.GONE);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// TODO: Implement this method
		if (item.getItemId() == android.R.id.home) {
			finish();
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		if (urlWebView.canGoBack()) {
			urlWebView.goBack();
		} else {
			finish();
		}
	}
}
