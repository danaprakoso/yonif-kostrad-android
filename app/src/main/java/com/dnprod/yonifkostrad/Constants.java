package com.dnprod.yonifkostrad;

public class Constants {
	public static final String HOST = "yonifkostrad.xyz";
	public static final String PHP_URL = "http://"+HOST+"/yonifkostrad/index.php";
	public static final String IMAGE_URL = "http://"+HOST+"/yonifkostrad/userdata/";
	public static final String USERDATA_URL = "http://"+HOST+"/yonifkostrad/userdata/";
	public static final String ROOT = "http://"+HOST+"/";
	public static final String NOTIFICATION_CHANNEL = "com.dnprod.yonifkostrad.NOTIFICATIONS";
	public static String EMAIL = "";
	public static int USER_ID = 0;
	public static int ROLE = 0;
}
