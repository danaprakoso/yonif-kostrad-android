package com.dnprod.yonifkostrad;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;

public class BackgroundService extends Service {
    public static BackgroundService instance;
    public static boolean started = false;
    BackgroundBinder binder = new BackgroundBinder();
    public static MediaPlayer mp;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (started) {
            return START_STICKY;
        }
        started = true;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, Constants.NOTIFICATION_CHANNEL);
        builder.setAutoCancel(false);
        builder.setContentTitle(getResources().getString(R.string.app_is_running));
        builder.setContentText(getResources().getString(R.string.app_description));
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, HomeActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));
        startForeground(1, builder.build());
        return START_STICKY;
    }

    public void play(int alarmType) {
        if (mp != null) {
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                }
                mp.release();
            } catch (Exception e) {}
            mp = null;
        }
        if (alarmType == 1) {
            mp = MediaPlayer.create(this, R.raw.alarm1);
        } else if (alarmType == 2) {
            mp = MediaPlayer.create(this, R.raw.alarm2);
        } else if (alarmType == 3) {
            mp = MediaPlayer.create(this, R.raw.alarm3);
        } else if (alarmType == 4) {
            mp = MediaPlayer.create(this, R.raw.alarm4);
        } else if (alarmType == 5) {
            mp = MediaPlayer.create(this, R.raw.alarm5);
        } else if (alarmType == 6) {
            mp = MediaPlayer.create(this, R.raw.alarm6);
        }
        mp.setLooping(true);
        mp.start();
    }

    public void stop() {
        if (mp != null) {
            try {
                if (mp.isPlaying()) {
                    mp.stop();
                }
                mp.release();
                mp = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class BackgroundBinder extends Binder {

        public BackgroundService getService() {
            return BackgroundService.this;
        }
    }
}
