package com.dnprod.yonifkostrad;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import java.util.ArrayList;
import org.json.JSONObject;
import com.dnprod.yonifkostrad.adapter.CommanderAdapter;
import org.json.JSONArray;

public class SelectCommanderActivity extends BaseActivity {
	RecyclerView commanderList;
	ArrayList<JSONObject> commanders;
	CommanderAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_commander);
		setTitle("Pilih Komandan");
		setSupportActionBar((Toolbar)findViewById(R.id.toolbar));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		commanderList = findViewById(R.id.commanders);
		commanderList.setLayoutManager(new LinearLayoutManager(this));
		commanderList.setItemAnimator(new DefaultItemAnimator());
		commanders = new ArrayList<>();
		adapter = new CommanderAdapter(this, commanders);
		commanderList.setAdapter(adapter);
		getCommanders();
	}
	
	public void getCommanders() {
		post(new Listener() {

				@Override
				public void onResponse(String response) {
					try {
						//showLog(response);
						JSONArray commandersJSON = new JSONArray(response);
						for (int i=0; i<commandersJSON.length(); i++) {
							commanders.add(commandersJSON.getJSONObject(i));
						}
						adapter.notifyDataSetChanged();
					} catch (Exception e) {}
				}
		}, Constants.PHP_URL+"/main/query", "cmd", "SELECT * FROM `users`");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
		}
		return false;
	}
}
