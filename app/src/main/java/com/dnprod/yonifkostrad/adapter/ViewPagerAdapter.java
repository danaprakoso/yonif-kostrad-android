package com.dnprod.yonifkostrad.adapter;

import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.ArrayList;

public class ViewPagerAdapter extends FragmentPagerAdapter
{
	ArrayList<Fragment> fragments;
	ArrayList<String> fragmentTitles;
	
	public ViewPagerAdapter(FragmentManager mgr) {
		super(mgr);
		fragments = new ArrayList<>();
		fragmentTitles = new ArrayList<>();
	}
	
	public void add(Fragment fr, String title) {
		fragments.add(fr);
		fragmentTitles.add(title);
	}

	@Override
	public int getCount()
	{
		// TODO: Implement this method
		return fragments.size();
	}

	@Override
	public Fragment getItem(int position)
	{
		// TODO: Implement this method
		return fragments.get(position);
	}

	@Override
	public CharSequence getPageTitle(int position)
	{
		// TODO: Implement this method
		return fragmentTitles.get(position);
	}
}
