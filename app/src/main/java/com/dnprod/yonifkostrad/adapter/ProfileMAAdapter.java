package com.dnprod.yonifkostrad.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import org.json.JSONObject;
import android.view.LayoutInflater;
import com.dnprod.yonifkostrad.R;
import android.widget.ImageView;
import android.widget.TextView;
import com.dnprod.yonifkostrad.Util;
import com.squareup.picasso.Picasso;
import android.net.Uri;
import com.dnprod.yonifkostrad.Constants;
import com.squareup.picasso.Target;
import android.graphics.drawable.Drawable;
import android.graphics.Bitmap;

public class ProfileMAAdapter extends RecyclerView.Adapter<ProfileMAAdapter.ViewHolder>
{
	Context context;
	ArrayList<JSONObject> users;

	public ProfileMAAdapter(Context ctx, ArrayList<JSONObject> users) {
		this.context = ctx;
		this.users = users;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup container, int p2)
	{
		// TODO: Implement this method
		return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.profile_ma, container, false));
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position)
	{
		// TODO: Implement this method
		try {
			JSONObject user = users.get(position);
			holder.nama.setText(Util.getString(user, "nama", "").trim());
			holder.pangkat.setText(Util.getString(user, "pangkat", "").trim());
			holder.nrp.setText(Util.getString(user, "nrp", "").trim());
			holder.ttl.setText(Util.getString(user, "ttl", "").trim());
			holder.jabatan.setText(Util.getString(user, "jabatan", "").trim());
			String profilePictureURL = Util.getString(user, "profile_picture", "").trim();
			Target target = new Target() {

				@Override
				public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom p2)
				{
					// TODO: Implement this method
					try {
						holder.profilePicture.setImageBitmap(Util.getCroppedBitmap(bitmap));
					} catch (Exception e) {}
				}

				@Override
				public void onBitmapFailed(Exception p1, Drawable p2)
				{
					// TODO: Implement this method
				}

				@Override
				public void onPrepareLoad(Drawable p1)
				{
					// TODO: Implement this method
				}
			};
			holder.profilePicture.setTag(target);
			if (!profilePictureURL.equals("")) {
				Picasso.get().load(Uri.parse(Constants.IMAGE_URL+profilePictureURL)).resize(128, 0).onlyScaleDown().into(target);
			} else {
				Picasso.get().load(R.drawable.profile_picture_placeholder).resize(128, 0).onlyScaleDown().into(target);
			}
		} catch (Exception e) {}
	}

	@Override
	public int getItemCount()
	{
		// TODO: Implement this method
		return users.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public ImageView profilePicture;
		public TextView nama, pangkat, nrp, ttl, jabatan;

		public ViewHolder(View view) {
			super(view);
			profilePicture = view.findViewById(R.id.profile_picture);
			nama = view.findViewById(R.id.nama);
			pangkat = view.findViewById(R.id.pangkat);
			nrp = view.findViewById(R.id.nrp);
			ttl = view.findViewById(R.id.ttl);
			jabatan = view.findViewById(R.id.jabatan);
		}
	}
}
