package com.dnprod.yonifkostrad.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.dnprod.yonifkostrad.R;
import java.util.ArrayList;
import org.json.JSONObject;
import com.squareup.picasso.Picasso;
import android.net.Uri;
import com.dnprod.yonifkostrad.Constants;
import com.dnprod.yonifkostrad.Util;
import android.text.Html;
import android.widget.LinearLayout;
import androidx.appcompat.app.AlertDialog;
import android.content.DialogInterface;
import com.dnprod.yonifkostrad.ViewVideoActivity;
import android.content.Intent;
import android.app.ProgressDialog;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder>
{
	Context context;
	ArrayList<JSONObject> videos;

	public VideoAdapter(Context ctx, ArrayList<JSONObject> videos) {
		this.context = ctx;
		this.videos = videos;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup container, int p2)
	{
		// TODO: Implement this method
		return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.video, container, false));
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position)
	{
		// TODO: Implement this method
		try {
			final int videoPosition = position;
			final JSONObject video = videos.get(position);
			Picasso.get().load(Uri.parse(Constants.IMAGE_URL+Util.getString(video, "screenshot", "").trim())).resize(256, 0).onlyScaleDown().into(holder.img);
			holder.name.setText(Html.fromHtml("Nama: "+Util.getString(video, "title", "").trim()));
			holder.date.setText(Html.fromHtml("Tanggal: "+Util.getString(video, "date", "").trim()));
			holder.uploader.setText(Html.fromHtml("Pengunggah: "+Util.getString(video, "uploader", "").trim()));
			holder.selectVideo.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {
						AlertDialog dialog = new AlertDialog.Builder(context)
							.setItems(new String[] {
								"Lihat", "Hapus"
							}, new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface p1, int position)
								{
									if (position == 0) {
										Intent i = new Intent(context, ViewVideoActivity.class);
										i.putExtra("url", Constants.USERDATA_URL+Util.getString(video, "video", "").trim());
										context.startActivity(i);
									} else if (position == 1) {
										AlertDialog dialog = new AlertDialog.Builder(context)
											.setMessage("Apakah Anda yakin ingin menghapus video ini?")
											.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

												@Override
												public void onClick(DialogInterface p1, int p2)
												{
													// TODO: Implement this method
													final ProgressDialog dialog = Util.createDialog(context, "Menghapus video...");
													dialog.show();
													Util.post(context, new Util.Listener() {

															@Override
															public void onResponse(String response)
															{
																// TODO: Implement this method
																dialog.dismiss();
																videos.remove(videoPosition);
																notifyDataSetChanged();
															}
													}, Constants.PHP_URL+"/user/delete_video", "id", ""+Util.getInt(video, "id", 0));
												}
											})
											.setNegativeButton("Tidak", null)
											.create();
										dialog.show();
									}
								}
							})
							.create();
						dialog.show();
					}
			});
		} catch (Exception e) {}
	}

	@Override
	public int getItemCount()
	{
		// TODO: Implement this method
		return videos.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public LinearLayout selectVideo;
		public ImageView img;
		public TextView name, date, uploader;

		public ViewHolder(View view) {
			super(view);
			selectVideo = view.findViewById(R.id.select_video);
			img = view.findViewById(R.id.img);
			name = view.findViewById(R.id.name);
			date = view.findViewById(R.id.date);
			uploader = view.findViewById(R.id.uploader);
		}
	}
}
