package com.dnprod.yonifkostrad.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import org.json.JSONObject;
import android.view.LayoutInflater;
import com.dnprod.yonifkostrad.R;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Target;
import android.graphics.drawable.Drawable;
import android.graphics.Bitmap;
import com.squareup.picasso.Picasso;
import android.net.Uri;
import com.dnprod.yonifkostrad.Constants;
import com.dnprod.yonifkostrad.Util;
import android.text.Html;
import android.widget.Button;
import androidx.appcompat.app.AlertDialog;
import android.content.DialogInterface;
import android.app.ProgressDialog;

public class CommanderAdapter extends RecyclerView.Adapter<CommanderAdapter.ViewHolder>
{
	Context context;
	ArrayList<JSONObject> commanders;
	
	public CommanderAdapter(Context ctx, ArrayList<JSONObject> commanders) {
		this.context = ctx;
		this.commanders = commanders;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup container, int p2)
	{
		// TODO: Implement this method
		return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.commander, container, false));
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position)
	{
		// TODO: Implement this method
		try {
			final JSONObject commander = commanders.get(position);
			Target target = new Target() {

				@Override
				public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom p2)
				{
					// TODO: Implement this method
					try {
						holder.profilePicture.setImageBitmap(Util.getCroppedBitmap(bitmap));
					} catch (Exception e) {}
				}

				@Override
				public void onBitmapFailed(Exception p1, Drawable p2)
				{
					// TODO: Implement this method
				}

				@Override
				public void onPrepareLoad(Drawable p1)
				{
					// TODO: Implement this method
				}
			};
			holder.profilePicture.setTag(target);
			String profilePicture = Util.getString(commander, "photo", "").trim();
			if (profilePicture.equals("")) {
				Picasso.get().load(R.drawable.profile_picture_placeholder).resize(128, 0).onlyScaleDown().into(target);
			} else {
				Picasso.get().load(Uri.parse(Constants.IMAGE_URL+profilePicture)).resize(128, 0).onlyScaleDown().into(target);
			}
			holder.name.setText(Html.fromHtml("<b>Komandan:</b> "+Util.getString(commander, "name", "").trim()));
			holder.description.setText(Html.fromHtml("<b>Deskripsi:</b> "+Util.getString(commander, "description", "").trim()));
			final int subscribedCommanderID = Util.readEncrypted(context, "subscribed_commander_id", 0);
			if (Util.getInt(commander, "id", 0) == subscribedCommanderID) {
				holder.subscribe.setText("Unsubscribe");
				holder.subscribe.setBackgroundResource(R.drawable.unsubscribe_button_bg);
			} else {
				holder.subscribe.setText("Subscribe");
				holder.subscribe.setBackgroundResource(R.drawable.subscribe_button_bg);
			}
			holder.subscribe.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {
						if (subscribedCommanderID == Util.getInt(commander, "id", 0)) {
							AlertDialog dialog = new AlertDialog.Builder(context)
								.setMessage("Apakah Anda yakin ingin berhenti menerima pemberitahuan dari komandan berikut?")
								.setTitle("Konfirmasi")
								.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface p1, int p2) {
										final ProgressDialog dialog = Util.createDialog(context, "Mengubah pengaturan...");
										dialog.show();
										Util.post(context, new Util.Listener() {

												@Override
												public void onResponse(String response) {
													Util.writeEncrypted(context, "subscribed_commander_id", 0);
													notifyDataSetChanged();
													dialog.dismiss();
												}
											}, Constants.PHP_URL+"/user/unsubscribe_alarm", "soldier_id", ""+Constants.USER_ID, "commander_id", ""+subscribedCommanderID);
									}
								})
								.setNegativeButton("Tidak", null)
								.create();
							dialog.show();
						} else {
							AlertDialog dialog = new AlertDialog.Builder(context)
								.setMessage("Apakah Anda yakin ingin menerima pemberitahuan dari komandan berikut?")
								.setTitle("Konfirmasi")
								.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface p1, int p2) {
										final ProgressDialog dialog = Util.createDialog(context, "Mengubah pengaturan...");
										dialog.show();
										Util.post(context, new Util.Listener() {

												@Override
												public void onResponse(String response) {
													Util.writeEncrypted(context, "subscribed_commander_id", Util.getInt(commander, "id", 0));
													notifyDataSetChanged();
													dialog.dismiss();
												}
											}, Constants.PHP_URL+"/user/subscribe_alarm", "soldier_id", ""+Constants.USER_ID, "old_commander_id", ""+subscribedCommanderID, "new_commander_id", ""+Util.getInt(commander, "id", 0));
									}
								})
								.setNegativeButton("Tidak", null)
								.create();
							dialog.show();
						}
					}
			});
		} catch (Exception e) {}
	}

	@Override
	public int getItemCount()
	{
		// TODO: Implement this method
		return commanders.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public ImageView profilePicture;
		public TextView name, description;
		public Button subscribe;
		
		public ViewHolder(View view) {
			super(view);
			profilePicture = view.findViewById(R.id.profile_picture);
			name = view.findViewById(R.id.name);
			description = view.findViewById(R.id.description);
			subscribe = view.findViewById(R.id.subscribe);
		}
	}
}
