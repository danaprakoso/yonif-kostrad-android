package com.dnprod.yonifkostrad.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import org.json.JSONObject;
import android.view.LayoutInflater;
import com.dnprod.yonifkostrad.R;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dnprod.yonifkostrad.Util;

import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import android.net.Uri;
import com.dnprod.yonifkostrad.Constants;
import android.content.Intent;
import com.dnprod.yonifkostrad.ViewImageActivity;
import androidx.appcompat.app.AlertDialog;
import android.content.DialogInterface;
import android.app.ProgressDialog;
import androidx.cardview.widget.CardView;
import com.dnprod.yonifkostrad.ViewProfileActivity;

public class PrivateMessageAdapter extends RecyclerView.Adapter<PrivateMessageAdapter.ViewHolder>
{
	Context context;
	ArrayList<JSONObject> messages;

	public PrivateMessageAdapter(Context ctx, ArrayList<JSONObject> messages) {
		this.context = ctx;
		this.messages = messages;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup container, int p2)
	{
		// TODO: Implement this method
		return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.message, container, false));
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position)
	{
		// TODO: Implement this method
		final int messagePosition = position;
		try {
			final JSONObject message = messages.get(position);
			int senderID = Util.getInt(message, "sender_id", 0);
			int userID = Constants.USER_ID;
			if (userID == senderID) {
				holder.opponentMessageContainer.setVisibility(View.GONE);
				holder.myMessageContainer.setVisibility(View.VISIBLE);
				String messageText = Util.getString(message, "message", "").trim();
				String imageURL = Util.getString(message, "image", "").trim();
				if (!imageURL.equals("")) {
					final String imgURL = Constants.IMAGE_URL+imageURL;
					Picasso.get().load(Uri.parse(imgURL)).resize(256, 0).onlyScaleDown().into(holder.myImage);
					holder.myImage.setVisibility(View.VISIBLE);
					holder.myMessage.setVisibility(View.GONE);
					holder.myImage.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View view) {
								AlertDialog dialog = new AlertDialog.Builder(context)
									.setItems(new String[] {
										"Lihat Gambar", "Hapus"
									}, new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface p1, final int position) {
											if (position == 0) {
												Intent i = new Intent(context, ViewImageActivity.class);
												i.putExtra("img_url", imgURL);
												context.startActivity(i);
											} else if (position == 1) {
												AlertDialog dialog = new AlertDialog.Builder(context)
													.setTitle("Konfirmasi")
													.setMessage("Apakah Anda yakin ingin menghapus pesan ini?")
													.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

														@Override
														public void onClick(DialogInterface p1, int p2) {
															final ProgressDialog dialog = Util.createDialog(context, "Menghapus pesan...");
															dialog.show();
															Util.post(context, new Util.Listener() {

																	@Override
																	public void onResponse(String response) {
																		dialog.dismiss();
																		messages.remove(messagePosition);
																		notifyDataSetChanged();
																	}
																}, Constants.PHP_URL+"/main/delete_by_id", "name", "messages", "id", ""+Util.getInt(message, "id", 0));
														}
													})
													.setNegativeButton("Tidak", null)
													.create();
												dialog.show();
											}
										}
									})
									.create();
								dialog.show();
							}
						});
				} else {
					holder.myMessage.setText(messageText);
					holder.myMessage.setVisibility(View.VISIBLE);
					holder.myImage.setVisibility(View.GONE);
					holder.myMessage.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View view) {
								AlertDialog dialog = new AlertDialog.Builder(context)
									.setItems(new String[] {
										"Hapus"
									}, new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface p1, final int position) {
											if (position == 0) {
												AlertDialog dialog = new AlertDialog.Builder(context)
													.setTitle("Konfirmasi")
													.setMessage("Apakah Anda yakin ingin menghapus pesan ini?")
													.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

														@Override
														public void onClick(DialogInterface p1, int p2) {
															final ProgressDialog dialog = Util.createDialog(context, "Menghapus pesan...");
															dialog.show();
															Util.post(context, new Util.Listener() {

																	@Override
																	public void onResponse(String response) {
																		dialog.dismiss();
																		messages.remove(messagePosition);
																		notifyDataSetChanged();
																	}
																}, Constants.PHP_URL+"/main/delete_by_id", "name", "messages", "id", ""+Util.getInt(message, "id", 0));
														}
													})
													.setNegativeButton("Tidak", null)
													.create();
												dialog.show();
											}
										}
									})
									.create();
								dialog.show();
							}
						});
				}
				holder.myName.setText(Util.getString(message, "sender_name", "").trim());
			} else {
				holder.myMessageContainer.setVisibility(View.GONE);
				holder.opponentMessageContainer.setVisibility(View.VISIBLE);
				String messageText = Util.getString(message, "message", "").trim();
				String imageURL = Util.getString(message, "image", "").trim();
				if (!imageURL.equals("")) {
					final String imgURL = Constants.IMAGE_URL+imageURL;
					Picasso.get().load(Uri.parse(Constants.IMAGE_URL+imageURL)).resize(256, 0).onlyScaleDown().into(holder.opponentImage);
					holder.opponentImage.setVisibility(View.VISIBLE);
					holder.opponentMessage.setVisibility(View.GONE);
					holder.opponentImage.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View view) {
								AlertDialog dialog = new AlertDialog.Builder(context)
									.setItems(new String[] {
										"Lihat Gambar", "Buka Profil", "Hapus"
									}, new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface p1, final int position) {
											if (position == 0) {
												Intent i = new Intent(context, ViewImageActivity.class);
												i.putExtra("img_url", imgURL);
												context.startActivity(i);
											} else if (position == 1) {
												Intent i = new Intent(context, ViewProfileActivity.class);
												i.putExtra("user_id", Util.getInt(message, "sender_id", 0));
												context.startActivity(i);
											} else if (position == 2) {
												AlertDialog dialog = new AlertDialog.Builder(context)
													.setTitle("Konfirmasi")
													.setMessage("Apakah Anda yakin ingin menghapus pesan ini?")
													.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

														@Override
														public void onClick(DialogInterface p1, int p2) {
															final ProgressDialog dialog = Util.createDialog(context, "Menghapus pesan...");
															dialog.show();
															Util.post(context, new Util.Listener() {

																	@Override
																	public void onResponse(String response) {
																		dialog.dismiss();
																		messages.remove(messagePosition);
																		notifyDataSetChanged();
																	}
																}, Constants.PHP_URL+"/main/delete_by_id", "name", "messages", "id", ""+Util.getInt(message, "id", 0));
														}
													})
													.setNegativeButton("Tidak", null)
													.create();
												dialog.show();
											}
										}
									})
									.create();
								dialog.show();
							}
						});
				} else {
					holder.opponentMessage.setText(messageText);
					holder.opponentMessage.setVisibility(View.VISIBLE);
					holder.opponentImage.setVisibility(View.GONE);
					holder.opponentMessage.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View view) {
								AlertDialog dialog = new AlertDialog.Builder(context)
									.setItems(new String[] {
										"Buka Profil", "Hapus"
									}, new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface p1, final int position) {
											if (position == 0) {
												Intent i = new Intent(context, ViewProfileActivity.class);
												i.putExtra("user_id", Util.getInt(message, "sender_id", 0));
												context.startActivity(i);
											} else if (position == 1) {
												AlertDialog dialog = new AlertDialog.Builder(context)
													.setTitle("Konfirmasi")
													.setMessage("Apakah Anda yakin ingin menghapus pesan ini?")
													.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

														@Override
														public void onClick(DialogInterface p1, int p2) {
															final ProgressDialog dialog = Util.createDialog(context, "Menghapus pesan...");
															dialog.show();
															Util.post(context, new Util.Listener() {

																	@Override
																	public void onResponse(String response) {
																		dialog.dismiss();
																		messages.remove(messagePosition);
																		notifyDataSetChanged();
																	}
																}, Constants.PHP_URL+"/main/delete_by_id", "name", "messages", "id", ""+Util.getInt(message, "id", 0));
														}
													})
													.setNegativeButton("Tidak", null)
													.create();
												dialog.show();
											}
										}
									})
									.create();
								dialog.show();
							}
						});
				}
				holder.opponentName.setText(Util.getString(message, "sender_name", "").trim());
			}
		} catch (Exception e) {
			Util.show(context, e.getMessage());
		}
	}

	@Override
	public int getItemCount()
	{
		// TODO: Implement this method
		return messages.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public LinearLayout myMessageContainer;
		public CardView opponentMessageContainer;
		public TextView myMessage, myName, opponentMessage, opponentName;
		public ImageView myImage, opponentImage;

		public ViewHolder(View view) {
			super(view);
			myMessageContainer = view.findViewById(R.id.my_message_container);
			opponentMessageContainer = view.findViewById(R.id.opponent_message_container);
			myMessage = view.findViewById(R.id.my_message);
			opponentMessage = view.findViewById(R.id.opponent_message);
			myName = view.findViewById(R.id.my_name);
			opponentName = view.findViewById(R.id.opponent_name);
			myImage = view.findViewById(R.id.my_image);
			opponentImage = view.findViewById(R.id.opponent_image);
		}
	}
}
