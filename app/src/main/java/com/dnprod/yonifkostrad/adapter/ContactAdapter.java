package com.dnprod.yonifkostrad.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dnprod.yonifkostrad.R;
import com.dnprod.yonifkostrad.Util;
import java.util.ArrayList;
import org.json.JSONObject;
import android.app.ProgressDialog;
import com.dnprod.yonifkostrad.Constants;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder>
{
	Context context;
	ArrayList<JSONObject> contacts;

	public ContactAdapter(Context ctx, ArrayList<JSONObject> contacts) {
		this.context = ctx;
		this.contacts = contacts;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup container, int p2)
	{
		// TODO: Implement this method
		return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.contact, container, false));
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position)
	{
		// TODO: Implement this method
		try {
			final int contactPosition = position;
			final JSONObject contact = contacts.get(position);
			holder.name.setText(Util.getString(contact, "name", "").trim());
			holder.phone.setText(Util.getString(contact, "phone", "").trim());
			holder.selectContact.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {
						AlertDialog dialog = new AlertDialog.Builder(context)
							.setItems(new String[] {
								"Panggil", "Hapus"
							}, new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface p1, final int position)
								{
									// TODO: Implement this method
									if (position == 0) {
										Intent intent = new Intent(Intent.ACTION_DIAL);
										intent.setData(Uri.parse("tel:"+Util.getString(contact, "phone", "").trim()));
										context.startActivity(intent);
									} else if (position == 1) {
										AlertDialog dialog = new AlertDialog.Builder(context)
											.setMessage("Apakah Anda yakin ingin menghapus kontak ini?")
											.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

												@Override
												public void onClick(DialogInterface p1, int p2)
												{
													// TODO: Implement this method
													final ProgressDialog dialog = Util.createDialog(context, "Menghapus kontak...");
													dialog.show();
													Util.post(context, new Util.Listener() {

															@Override
															public void onResponse(String response)
															{
																// TODO: Implement this method
																dialog.dismiss();
																contacts.remove(contactPosition);
																notifyDataSetChanged();
															}
													}, Constants.PHP_URL+"/main/execute", "cmd", "DELETE FROM `contacts` WHERE `id`="+Util.getInt(contact, "id", 0));
												}
											})
											.setNegativeButton("Tidak", null)
											.create();
										dialog.show();
									}
								}
							})
							.create();
						dialog.show();
					}
			});
		} catch (Exception e) {}
	}

	@Override
	public int getItemCount()
	{
		// TODO: Implement this method
		return contacts.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public LinearLayout selectContact;
		public ImageView img;
		public TextView name, phone;

		public ViewHolder(View view) {
			super(view);
			selectContact = view.findViewById(R.id.select_contact);
			img = view.findViewById(R.id.img);
			name = view.findViewById(R.id.name);
			phone = view.findViewById(R.id.phone);
		}
	}
}
