package com.dnprod.yonifkostrad.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.dnprod.yonifkostrad.R;
import java.util.ArrayList;
import org.json.JSONObject;
import com.squareup.picasso.Picasso;
import android.net.Uri;
import com.dnprod.yonifkostrad.Constants;
import com.dnprod.yonifkostrad.Util;

public class DisasterAdapter extends RecyclerView.Adapter<DisasterAdapter.ViewHolder>
{
	Context context;
	ArrayList<JSONObject> disasters;
	
	public DisasterAdapter(Context ctx, ArrayList<JSONObject> disasters) {
		this.context = ctx;
		this.disasters = disasters;
	}

	@Override
	public DisasterAdapter.ViewHolder onCreateViewHolder(ViewGroup container, int p2)
	{
		// TODO: Implement this method
		return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.disaster, container, false));
	}

	@Override
	public void onBindViewHolder(DisasterAdapter.ViewHolder holder, int position)
	{
		// TODO: Implement this method
		try {
			JSONObject disaster = disasters.get(position);
			Picasso.get().load(Uri.parse(Constants.IMAGE_URL+Util.getString(disaster, "image", "").trim())).resize(256, 0).onlyScaleDown().into(holder.img);
			holder.name.setText(Util.getString(disaster, "name", "").trim());
		} catch (Exception e) {}
	}

	@Override
	public int getItemCount()
	{
		// TODO: Implement this method
		return disasters.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public ImageView img;
		public TextView name;
		
		public ViewHolder(View view) {
			super(view);
			img = view.findViewById(R.id.img);
			name = view.findViewById(R.id.name);
		}
	}
}
