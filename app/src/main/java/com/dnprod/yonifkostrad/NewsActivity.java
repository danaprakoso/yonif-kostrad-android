package com.dnprod.yonifkostrad;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import androidx.viewpager.widget.ViewPager;
import com.dnprod.yonifkostrad.adapter.ViewPagerAdapter;
import com.dnprod.yonifkostrad.newsfragments.NewsFragment;

public class NewsActivity extends BaseActivity {
	TabLayout tabs;
	ViewPager viewPager;
	ViewPagerAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news);
		setTitle("Berita");
		setSupportActionBar((Toolbar)findViewById(R.id.toolbar));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		tabs = findViewById(R.id.tabs);
		viewPager = findViewById(R.id.viewpager);
		adapter = new ViewPagerAdapter(getSupportFragmentManager());
		{
			NewsFragment fr = new NewsFragment();
			Bundle args = new Bundle();
			args.putString("url", "https://twitter.com/bnpb_indonesia");
			fr.setArguments(args);
			adapter.add(fr, "Twitter");
		}
		{
			NewsFragment fr = new NewsFragment();
			Bundle args = new Bundle();
			args.putString("url", "https://www.bmkg.go.id/");
			fr.setArguments(args);
			adapter.add(fr, "BMKG");
		}
		{
			NewsFragment fr = new NewsFragment();
			Bundle args = new Bundle();
			args.putString("url", "https://bnpb.go.id/");
			fr.setArguments(args);
			adapter.add(fr, "BNPB");
		}
		viewPager.setAdapter(adapter);
		tabs.setupWithViewPager(viewPager);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
		}
		return false;
	}
}
