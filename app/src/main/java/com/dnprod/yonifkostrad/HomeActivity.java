package com.dnprod.yonifkostrad;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.dnprod.yonifkostrad.fragments.BateraiMAFragment;
import com.dnprod.yonifkostrad.fragments.BateraiPFragment;
import com.dnprod.yonifkostrad.fragments.BateraiQFragment;
import com.dnprod.yonifkostrad.fragments.BateraiRFragment;
import com.dnprod.yonifkostrad.fragments.HomeFragment;
import com.dnprod.yonifkostrad.fragments.VideoFragment;
import com.dnprod.yonifkostrad.fragments.ContactFragment;

import androidx.appcompat.app.AlertDialog;
import android.content.DialogInterface;

public class HomeActivity extends BaseActivity {
	private final int SIGN_UP = 1;
	TextView emailView;
	public HomeFragment homeFragment;
	//public SSKSiagaFragment sskFragment;
	public BateraiPFragment bateraiPFragment;
	public BateraiQFragment bateraiQFragment;
	public BateraiRFragment bateraiRFragment;
	public BateraiMAFragment bateraiMAFragment;
	//public DisasterFragment disasterFragment;
	public VideoFragment videoFragment;
	public ContactFragment contactFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		updateFCMID();
		int userID = Constants.USER_ID;
		if (userID != 0) {
			updateFCMToken();
			updatePushyToken();
		}
		setContentView(R.layout.activity_home);
		setTitle("");
		Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
		/*AlertDialog dialog = new AlertDialog.Builder(this)
			.setCancelable(false)
			.setMessage("Aplikasi ini adalah aplikasi demo.")
			.setPositiveButton("OK", null)
			.create();
		dialog.show();*/
		final DrawerLayout drawer = findViewById(R.id.drawer_layout);
		NavigationView navigationView = findViewById(R.id.nav_view);
		emailView = navigationView.getHeaderView(0).findViewById(R.id.email);
		emailView.setText(readEncrypted("email", ""));
		navigationView.setItemIconTintList(null);
		navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

				@Override
				public boolean onNavigationItemSelected(MenuItem item)
				{
					drawer.closeDrawer(GravityCompat.START);
					int id = item.getItemId();
					if (id == R.id.home) {
						selectFragment(homeFragment);
					}/* else if (id == R.id.ssk_siaga) {
						selectFragment(sskFragment);
					}*/ else if (id == R.id.ran_ops) {
						Intent i = new Intent(HomeActivity.this, ViewDocumentActivity.class);
						i.putExtra("title", "Ran Operasional");
						i.putExtra("url", "https://docs.google.com/spreadsheets/d/1huKBSd4II_QobIthyCfXdDaaehrl7xHwv73I3WC2XBo/edit#gid=1286674887");
						startActivity(i);
					} else if (id == R.id.baterai_p) {
						selectFragment(bateraiPFragment);
					} else if (id == R.id.baterai_q) {
						selectFragment(bateraiQFragment);
					} else if (id == R.id.baterai_r) {
						selectFragment(bateraiRFragment);
					} else if (id == R.id.baterai_ma) {
						selectFragment(bateraiMAFragment);
					} else if (id == R.id.info_bmkg) {
						Intent i = new Intent(HomeActivity.this, ViewDocumentActivity.class);
						i.putExtra("title", "Info BMKG");
						i.putExtra("url", "https://www.bmkg.go.id/");
						startActivity(i);
					} else if (id == R.id.bnpb_indonesia) {
						Intent i = new Intent(HomeActivity.this, ViewDocumentActivity.class);
						i.putExtra("title", "BNPB Indonesia");
						i.putExtra("url", "https://twitter.com/bnpb_indonesia");
						startActivity(i);
					} else if (id == R.id.bnpb) {
						Intent i = new Intent(HomeActivity.this, ViewDocumentActivity.class);
						i.putExtra("title", "BNPB Indonesia");
						i.putExtra("url", "https://bnpb.go.id/");
						startActivity(i);
					}/* else if (id == R.id.jenis_bencana) {
						selectFragment(disasterFragment);
					}*/ else if (id == R.id.chat) {
						startActivity(new Intent(HomeActivity.this, ChatActivity.class));
					} else if (id == R.id.videos) {
						selectFragment(videoFragment);
					} else if (id == R.id.contact) {
						selectFragment(contactFragment);
					} /* else if (id == R.id.privacy_policy) {
						Intent i = new Intent(HomeActivity.this, ViewDocumentActivity.class);
						i.putExtra("title", "Kebijakan Privasi");
						i.putExtra("url", "https://"+Constants.HOST+"/privacy_policy.html");
						startActivity(i);
					}*/ else if (id == R.id.logout) {
						logout();
					}
					return false;
				}
			});
		ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.open_drawer, R.string.close_drawer) {

            @Override
            public void onDrawerClosed(View drawerView)
			{
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView)
			{
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
		homeFragment = new HomeFragment();
		//sskFragment = new SSKSiagaFragment();
		bateraiPFragment = new BateraiPFragment();
		bateraiQFragment = new BateraiQFragment();
		bateraiRFragment = new BateraiRFragment();
		bateraiMAFragment = new BateraiMAFragment();
		//disasterFragment = new DisasterFragment();
		videoFragment = new VideoFragment();
		contactFragment = new ContactFragment();
		selectFragment(homeFragment);
		getLocation(new BaseActivity.LocationUpdateListener() {

				@Override
				public void onLocationUpdate(double lat, double lng) {
					post(new Listener() {

							@Override
							public void onResponse(String response) {
							}
					}, Constants.PHP_URL+"/main/execute", "cmd", "UPDATE `users` SET `lat`="+lat+", `lng`="+lng+" WHERE `id`="+Constants.USER_ID);
				}
		});
	}
	
	public void startAlarmIntent() {
		int role = Constants.ROLE;
		if (role == 0) {
			Intent i = new Intent(this, AlertCommanderActivity.class);
			startActivity(i);
		} else if (role == 1) {
			Intent i = new Intent(this, AlertSoldierActivity.class);
			startActivity(i);
		}
	}
	
	public void logout() {
		AlertDialog dialog = new AlertDialog.Builder(HomeActivity.this)
			.setTitle("Konfirmasi")
			.setMessage("Apakah Anda yakin ingin logout?")
			.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface p1, int p2) {
					Constants.USER_ID = 0;
					Constants.ROLE = 0;
				}
			})
			.setNegativeButton("Tidak", null)
			.create();
		dialog.show();
	}
	
	public void selectFragment(Fragment fr) {
		if (fr instanceof HomeFragment) {
			setTitle("Beranda");
		}/* else if (fr instanceof SSKSiagaFragment) {
			setTitle("SSK Siap Siaga");
		}*/ else if (fr instanceof BateraiPFragment) {
			setTitle("Baterai P");
		} else if (fr instanceof BateraiQFragment) {
			setTitle("Baterai Q");
		} else if (fr instanceof BateraiRFragment) {
			setTitle("Baterai R");
		} else if (fr instanceof BateraiMAFragment) {
			setTitle("Baterai MA");
		}/* else if (fr instanceof DisasterFragment) {
			setTitle("Jenis-Jenis Bencana");
		}*/ else if (fr instanceof ContactFragment) {
			setTitle("Kontak");
		}
		getSupportFragmentManager().beginTransaction().replace(R.id.content_home, fr).commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.share) {
			String shareBody = "Unduh aplikasi "+getAppName()+" dengan mengakses link berikut https://play.google.com/store/apps/details?id="+getPackageName();
			Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
			sharingIntent.setType("text/plain");
			sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getAppName());
			sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
			startActivity(Intent.createChooser(sharingIntent, "Sebarkan via"));
		} else if (id == R.id.profile) {
			Intent i = new Intent(this, WebViewActivity.class);
			i.putExtra("title", "Nominatif Batalyon");
			i.putExtra("url", "http://docs.google.com/spreadsheets/d/1kRUH9hwFV9Fr9uoPnSxrOtGXbMWTEX3I/edit?usp=drive_web&ouid=107435193818545314033&dls=true");
			startActivity(i);
		} else if (id == R.id.notification) {
			if (Constants.USER_ID == 0) {
				startActivityForResult(new Intent(HomeActivity.this, SignupActivity.class), SIGN_UP);
			} else {
				startAlarmIntent();
			}
		} else if (id == R.id.exit) {
			logout();
		}
		return false;
	}

	@Override
	protected void onStart() {
		super.onStart();
		Intent i = new Intent(this, BackgroundService.class);
		if (!BackgroundService.started) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				ContextCompat.startForegroundService(this, i);
			} else {
				startService(i);
			}
		}
	}

	@Override
	public void onBackPressed() {
		FirebaseAuth.getInstance().signOut();
		finish();
	}
}
