package com.dnprod.yonifkostrad;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.dnprod.yonifkostrad.adapter.PrivateMessageAdapter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;

public class PrivateMessageActivity extends BaseActivity {
	private final int SIGN_UP = 1;
	private final int TAKE_PHOTO = 2;
	private final int PICK_IMAGE_FROM_GALLERY = 3;
	LinearLayout loginContainer;
	RecyclerView messageList;
	ArrayList<JSONObject> messages;
	PrivateMessageAdapter adapter;
	EditText messageField;
	LinearLayoutManager lm;
	File photoFile;
	int start = 0;
	int length = 20;
	int userID = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_private_message);
		setTitle("Pesan Pribadi");
		setSupportActionBar((Toolbar)findViewById(R.id.toolbar));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		userID = getIntent().getIntExtra("user_id", 0);
		loginContainer = findViewById(R.id.login_container);
		messageList = findViewById(R.id.messages);
		messageField = findViewById(R.id.message);
		if (Constants.USER_ID == 0) {
			loginContainer.setVisibility(View.VISIBLE);
		} else {
			loginContainer.setVisibility(View.GONE);
		}
		lm = new LinearLayoutManager(this);
		messageList.setLayoutManager(lm);
		messageList.setItemAnimator(new DefaultItemAnimator());
		messages = new ArrayList<>();
		adapter = new PrivateMessageAdapter(this, messages);
		messageList.setAdapter(adapter);
		messageList.addOnScrollListener(new RecyclerView.OnScrollListener() {

				@Override
				public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
					super.onScrolled(recyclerView, dx, dy);
					int pastVisibleItems = lm.findFirstCompletelyVisibleItemPosition();
					if (pastVisibleItems == 0) {
						getMessages(false);
					}
				}
			});
		start = 0;
		getMessages(true);
	}

	public void getMessages(final boolean needScroll) {
		post(new Listener() {

				@Override
				public void onResponse(String response) {
					try {
						JSONArray messagesJSON = new JSONArray(response);
						for (int i=0; i<messagesJSON.length(); i++) {
							messages.add(0, messagesJSON.getJSONObject(i));
						}
						adapter.notifyDataSetChanged();
						if (needScroll) {
							messageList.scrollToPosition(messages.size()-1);
						}
						start += length;
					} catch (Exception e) {}
				}
			}, Constants.PHP_URL+"/user/get_private_messages", "my_user_id", ""+Constants.USER_ID, "opponent_user_id", ""+userID, "start", ""+start, "length", ""+length);
	}

	public void sendMessage(View view) {
		final String message = messageField.getText().toString().trim();
		if (message.equals("")) {
			show("Mohon masukkan isi pesan");
			return;
		}
		messageField.setText("");
		hideKeyboard();
		post(new Listener() {

				@Override
				public void onResponse(String response) {
					try {
						JSONObject obj = new JSONObject(response);
						messages.add(obj);
						adapter.notifyDataSetChanged();
						messageList.scrollToPosition(messages.size()-1);
					} catch (Exception e) {}
				}
			}, Constants.PHP_URL+"/user/send_message_to_user", "message", message, "sender_id", ""+Constants.USER_ID, "receiver_id", ""+userID, "date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	}

	public void login(View view) {
		startActivityForResult(new Intent(this, SignupActivity.class), SIGN_UP);
	}

	public void attach(View view) {
		AlertDialog dialog = new AlertDialog.Builder(this)
			.setTitle("Ambil foto dari...")
			.setItems(new String[] {
				"Kamera", "Galeri"
			}, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface p1, int position) {
					if (position == 0) {
						Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
							photoFile = null;
							try {
								photoFile = createImageFile();
							} catch (Exception e) {
								e.printStackTrace();
							}
							if (photoFile != null) {
								Uri photoURI = FileProvider.getUriForFile(PrivateMessageActivity.this, getPackageName()+".fileprovider", photoFile);
								takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
								startActivityForResult(takePictureIntent, TAKE_PHOTO);
							}
						}
					} else if (position == 1) {
						Intent intent = new Intent();
						intent.setType("image/*");
						intent.setAction(Intent.ACTION_GET_CONTENT);
						startActivityForResult(Intent.createChooser(intent, "Pilih gambar..."), PICK_IMAGE_FROM_GALLERY);
					}
				}
			})
			.create();
		dialog.show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
		}
		return false;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == SIGN_UP) {
				loginContainer.setVisibility(View.GONE);
			} else if (requestCode == TAKE_PHOTO) {
				uploadPhoto();
			} else if (requestCode == PICK_IMAGE_FROM_GALLERY) {
				try {
					photoFile = new File(getFilesDir(), UUID.randomUUID().toString()+".png");
					InputStream stream = getContentResolver().openInputStream(data.getData());
					FileOutputStream fos = new FileOutputStream(photoFile);
					int read;
					byte[] buffer = new byte[8192];
					while ((read = stream.read(buffer)) != -1) {
						fos.write(buffer, 0, read);
					}
					fos.flush();
					fos.close();
					stream.close();
				} catch (Exception e) {}
				uploadPhoto();
			}
		}
	}

	public void uploadPhoto() {
		post(new Listener() {

				@Override
				public void onResponse(String response) {
					try {
						show(response);
						JSONObject obj = new JSONObject(response);
						messages.add(obj);
						adapter.notifyDataSetChanged();
						messageList.scrollToPosition(messages.size()-1);
					} catch (Exception e) {}
				}
			}, Constants.PHP_URL+"/user/send_image_to_user", "sender_id", ""+Constants.USER_ID, "receiver_id", ""+userID, "file", UUID.randomUUID().toString()+".jpg", "image/jpeg", photoFile.getAbsolutePath(), "date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	}
}
