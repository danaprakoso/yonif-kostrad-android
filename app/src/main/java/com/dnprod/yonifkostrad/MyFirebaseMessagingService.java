package com.dnprod.yonifkostrad;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
	public static final String ACTION_PUBLIC_MESSAGE = "publicmessage";
	public static final String ACTION_PRIVATE_MESSAGE = "privatemessage";
	public static final int NOTIFICATION_TYPE_ALARM = 1;

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public void onNewToken(@NonNull String s) {
		super.onNewToken(s);
	}

	@Override
	public void onMessageReceived(RemoteMessage message) {
		super.onMessageReceived(message);
		try {
			int notificationType = Integer.parseInt(message.getData().get("notification_type"));
			int showNotification = Integer.parseInt(message.getData().get("show_notification"));
			int receiveAlerts = Integer.parseInt(message.getData().get("receive_alerts"));
			if (notificationType == NOTIFICATION_TYPE_ALARM) {
				int alarmType = Integer.parseInt(message.getData().get("alarm_type"));
				int alarmOn = Integer.parseInt(message.getData().get("alarm_on"));
				if (alarmOn == 1) {
					BackgroundService.instance.play(alarmType);
				} else if (alarmOn == 0) {
					BackgroundService.instance.stop();
				}
				if (showNotification == 1) {
					String title = message.getData().get("title");
					String body = message.getData().get("body");
					if (title == null || title.trim().equals("")) {
						title = getResources().getString(R.string.app_is_running);
					}
					if (body == null || body.trim().equals("")) {
						body = getResources().getString(R.string.app_description);
					}
					NotificationCompat.Builder builder = new NotificationCompat.Builder(MyFirebaseMessagingService.this, Constants.NOTIFICATION_CHANNEL)
							.setSmallIcon(R.drawable.ic_launcher)
							.setContentTitle(title)
							.setContentText(body)
							.setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, HomeActivity.class), PendingIntent.FLAG_UPDATE_CURRENT))
							.setPriority(NotificationCompat.PRIORITY_DEFAULT);
					if (receiveAlerts == 0) {
						builder.setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE+"://"+getPackageName()+"/"+R.raw.notification_sound));
					}
					NotificationManager mgr = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
					mgr.notify(1, builder.build());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
