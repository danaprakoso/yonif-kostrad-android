package com.dnprod.yonifkostrad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;
import android.app.NotificationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.content.ContentResolver;

public class PushReceiver extends BroadcastReceiver {
	public static final int NOTIFICATION_TYPE_ALARM = 1;
	MediaPlayer mp = null;

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (action.equals("pushy.me")) {
			try {
				int notificationType = intent.getIntExtra("notification_type", 0);
				int showNotification = intent.getIntExtra("show_notification", 0);
				int receiveAlerts = intent.getIntExtra("receive_alerts", 0);
				if (notificationType == NOTIFICATION_TYPE_ALARM) {
					int alarmType = intent.getIntExtra("alarm_type", 0);
					int alarmOn = intent.getIntExtra("alarm_on", 0);
					if (alarmOn == 1) {
						if (mp != null) {
							try {
								if (mp.isPlaying()) {
									mp.stop();
								}
								mp.release();
							} catch (Exception e) {}
							mp = null;
						}
						if (alarmType == 1) {
							mp = MediaPlayer.create(context, R.raw.alarm1);
						} else if (alarmType == 2) {
							mp = MediaPlayer.create(context, R.raw.alarm2);
						} else if (alarmType == 3) {
							mp = MediaPlayer.create(context, R.raw.alarm3);
						} else if (alarmType == 4) {
							mp = MediaPlayer.create(context, R.raw.alarm4);
						} else if (alarmType == 5) {
							mp = MediaPlayer.create(context, R.raw.alarm5);
						} else if (alarmType == 6) {
							mp = MediaPlayer.create(context, R.raw.alarm6);
						}
						mp.setLooping(true);
						mp.start();
					} else if (alarmOn == 0) {
						if (mp != null) {
							try {
								if (mp.isPlaying()) {
									mp.stop();
								}
								mp.release();
							} catch (Exception e) {}
							mp = null;
						}
					}
					if (showNotification == 1) {
						String title = intent.getStringExtra("title");
						String body = intent.getStringExtra("body");
						NotificationCompat.Builder builder = new NotificationCompat.Builder(context, Constants.NOTIFICATION_CHANNEL)
							.setSmallIcon(R.drawable.ic_launcher)
							.setContentTitle(title)
							.setContentText(body)
							.setPriority(NotificationCompat.PRIORITY_DEFAULT);
						if (receiveAlerts == 0) {
							builder.setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE+"://"+context.getPackageName()+"/"+R.raw.notification_sound));
						}
						NotificationManager mgr = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
						mgr.notify(1, builder.build());
					}
				}
			} catch (Exception e) {}
		}
	}
}
