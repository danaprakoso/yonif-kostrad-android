package com.dnprod.yonifkostrad;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.squareup.picasso.Picasso;
import android.app.ProgressDialog;
import org.json.JSONObject;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.util.UUID;

public class SignupActivity extends BaseActivity {
	private final int TAKE_PHOTO = 1;
	private final int PICK_IMAGE_FROM_GALLERY = 2;
	Spinner birthYearList;
	EditText nameField, descriptionField;
	RadioGroup roleView, genderView, commentsView, privateChatsView, notificationsView;
	RadioButton commander, user, male, allowComments, allowPrivateChats, iconAndSound;
	ImageView profilePictureView;
	File profilePictureFile = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);
		birthYearList = findViewById(R.id.birth_year);
		male = findViewById(R.id.male);
		roleView = findViewById(R.id.role);
		commander = findViewById(R.id.commander);
		user = findViewById(R.id.user);
		allowComments = findViewById(R.id.allow_comments);
		allowPrivateChats = findViewById(R.id.allow_private_chats);
		iconAndSound = findViewById(R.id.icon_and_sound);
		nameField = findViewById(R.id.name);
		descriptionField = findViewById(R.id.description);
		commentsView = findViewById(R.id.comments);
		privateChatsView = findViewById(R.id.private_chats);
		profilePictureView = findViewById(R.id.profile_picture);
		genderView = findViewById(R.id.gender);
		commentsView = findViewById(R.id.gender);
		privateChatsView = findViewById(R.id.private_chats);
		notificationsView = findViewById(R.id.notifications);
		male.setChecked(true);
		allowComments.setChecked(true);
		allowPrivateChats.setChecked(true);
		iconAndSound.setChecked(true);
		ArrayList<String> birthYears = new ArrayList<>();
		int currentYear = Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date()));
		int startYear = currentYear-50;
		int endYear = currentYear+50;
		int birthYearIndex = 0;
		int j = 0;
		for (int i=startYear; i<=endYear; i++) {
			birthYears.add(""+i);
			if (i == currentYear) {
				birthYearIndex = j-25;
			}
			j++;
		}
		ArrayAdapter<String> birthYearAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, birthYears);
		birthYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		birthYearList.setAdapter(birthYearAdapter);
		birthYearList.setSelection(birthYearIndex);
		commander.setChecked(true);
	}
	
	public void signup(View view) {
		final int role = Util.getRadioButtonIndex(roleView);
		final String name = nameField.getText().toString().trim();
		final String description = descriptionField.getText().toString().trim();
		final int birthYear = Integer.parseInt((String)birthYearList.getSelectedItem());
		final int gender = Util.getRadioButtonIndex(genderView);
		final int comments = Util.getRadioButtonIndex(commentsView);
		final int privateChats = Util.getRadioButtonIndex(privateChatsView);
		final int notifications = Util.getRadioButtonIndex(notificationsView);
		if (name.equals("") || description.equals("")) {
			show("Mohon masukkan data lengkap");
			return;
		}
		final ProgressDialog dialog = createDialog("Mendaftar...");
		dialog.show();
		if (profilePictureFile == null) {
			post(new Listener() {

				@Override
				public void onResponse(String response) {
					dialog.dismiss();
					try {
						JSONObject obj = new JSONObject(response);
						JSONObject data = obj.getJSONObject("data");
						int userID = getInt(data, "user_id", 0);
						int role = Constants.ROLE;
						Constants.ROLE = role;
						Constants.USER_ID = userID;
						updateFCMID();
						if (userID != 0) {
							updatePushyToken();
						}
					} catch (Exception e) {
						show(e.getMessage());
					}
					setResult(RESULT_OK);
					finish();
				}
			}, Constants.PHP_URL+"/user/update_profile", "email", Constants.EMAIL, "role", ""+role, "name", name, "description", description, "birth_year", ""+birthYear, "gender", ""+gender, "allow_comments", ""+comments, "allow_private_chats", ""+privateChats, "receive_alerts", ""+notifications);
		} else {
			post(new Listener() {

				@Override
				public void onResponse(String response) {
					dialog.dismiss();
					try {
						JSONObject obj = new JSONObject(response);
						JSONObject data = obj.getJSONObject("data");
						int userID = getInt(data, "user_id", 0);
						int role = getInt(data, "role", 0);
						Constants.ROLE = role;
						Constants.USER_ID = userID;
						updateFCMID();
						if (userID != 0) {
							updatePushyToken();
						}
					} catch (Exception e) {
						show(e.getMessage());
					}
					setResult(RESULT_OK);
					finish();
				}
			}, Constants.PHP_URL+"/user/update_profile", "email", Constants.EMAIL, "role", ""+role, "name", name, "description", description, "birth_year", ""+birthYear, "gender", ""+gender, "allow_comments", ""+comments, "allow_private_chats", ""+privateChats, "receive_alerts", ""+notifications, "file", UUID.randomUUID().toString()+".jpg", "image/jpeg", profilePictureFile.getAbsolutePath());
		}
	}
	
	public void selectCommander(View view) {
		
	}
	
	public void changeProfilePicture(View view) {
		AlertDialog dialog = new AlertDialog.Builder(this)
			.setTitle("Ambil foto dari...")
			.setItems(new String[] {
				"Kamera", "Galeri"
			}, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface p1, int position) {
					if (position == 0) {
						Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
							File photoFile = null;
							try {
								photoFile = createImageFile();
								profilePictureFile = photoFile;
							} catch (Exception e) {
								e.printStackTrace();
							}
							if (photoFile != null) {
								Uri photoURI = FileProvider.getUriForFile(SignupActivity.this, getPackageName()+".fileprovider", photoFile);
								takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
								startActivityForResult(takePictureIntent, TAKE_PHOTO);
							}
						}
					} else if (position == 1) {
						Intent intent = new Intent();
						intent.setType("image/*");
						intent.setAction(Intent.ACTION_GET_CONTENT);
						startActivityForResult(Intent.createChooser(intent, "Pilih gambar..."), PICK_IMAGE_FROM_GALLERY);
					}
				}
			})
			.create();
		dialog.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == TAKE_PHOTO) {
				Picasso.get().load(profilePictureFile).resize(256, 0).onlyScaleDown().into(profilePictureView);
			} else if (requestCode == PICK_IMAGE_FROM_GALLERY) {
				Picasso.get().load(data.getData()).resize(256, 0).onlyScaleDown().into(profilePictureView);
				try {
					profilePictureFile = new File(getFilesDir(), UUID.randomUUID().toString()+".png");
					InputStream stream = getContentResolver().openInputStream(data.getData());
					FileOutputStream fos = new FileOutputStream(profilePictureFile);
					int read;
					byte[] buffer = new byte[8192];
					while ((read = stream.read(buffer)) != -1) {
						fos.write(buffer, 0, read);
					}
					fos.flush();
					fos.close();
					stream.close();
				} catch (Exception e) {}
			}
		}
	}
}
