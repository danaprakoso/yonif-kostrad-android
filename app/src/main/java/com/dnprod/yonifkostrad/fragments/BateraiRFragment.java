package com.dnprod.yonifkostrad.fragments;

import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.view.View;
import com.dnprod.yonifkostrad.HomeActivity;
import com.dnprod.yonifkostrad.R;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import java.util.ArrayList;
import org.json.JSONObject;

import com.dnprod.yonifkostrad.BaseActivity;
import com.dnprod.yonifkostrad.Constants;
import org.json.JSONArray;
import androidx.core.widget.NestedScrollView;
import com.dnprod.yonifkostrad.adapter.ProfileRAdapter;
import android.widget.RelativeLayout;

public class BateraiRFragment extends Fragment {
	View view;
	HomeActivity activity;
	RecyclerView profileList;
	ArrayList<JSONObject> profiles;
	ProfileRAdapter adapter;
	NestedScrollView scroll;
	RelativeLayout progress;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// TODO: Implement this method
		view = inflater.inflate(R.layout.fragment_baterai_r, container, false);
		profileList = view.findViewById(R.id.profiles);
		progress = view.findViewById(R.id.progress);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO: Implement this method
		super.onActivityCreated(savedInstanceState);
		activity = (HomeActivity)getActivity();
		profileList.setLayoutManager(new LinearLayoutManager(activity));
		profileList.setItemAnimator(new DefaultItemAnimator());
		profiles = new ArrayList<>();
		adapter = new ProfileRAdapter(activity, profiles);
		profileList.setAdapter(adapter);
		getProfiles();
	}

	public void getProfiles() {
		progress.setVisibility(View.VISIBLE);
		activity.post(new BaseActivity.Listener() {

				@Override
				public void onResponse(String response) {
					try {
						JSONArray profilesJSON = new JSONArray(response);
						for (int i=0; i<profilesJSON.length(); i++) {
							JSONObject profileJSON = profilesJSON.getJSONObject(i);
							profiles.add(profileJSON);
						}
						adapter.notifyDataSetChanged();
						progress.setVisibility(View.GONE);
					} catch (Exception e) {}
				}
			}, Constants.PHP_URL+"/main/get_by_id_name_string", "name", "nominatif_batalyon", "id_name", "type", "id", "r");
	}
}

