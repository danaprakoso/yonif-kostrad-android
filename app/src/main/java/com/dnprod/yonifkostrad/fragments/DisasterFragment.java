package com.dnprod.yonifkostrad.fragments;

import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.view.View;
import com.dnprod.yonifkostrad.HomeActivity;
import com.dnprod.yonifkostrad.R;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import java.util.ArrayList;
import org.json.JSONObject;
import com.dnprod.yonifkostrad.adapter.DisasterAdapter;
import com.dnprod.yonifkostrad.BaseActivity;
import com.dnprod.yonifkostrad.Constants;
import org.json.JSONArray;

public class DisasterFragment extends Fragment {
	View view;
	HomeActivity activity;
	RecyclerView disasterList;
	ArrayList<JSONObject> disasters;
	DisasterAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// TODO: Implement this method
		view = inflater.inflate(R.layout.fragment_disaster, container, false);
		disasterList = view.findViewById(R.id.disasters);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO: Implement this method
		super.onActivityCreated(savedInstanceState);
		activity = (HomeActivity)getActivity();
		disasterList.setLayoutManager(new LinearLayoutManager(activity));
		disasterList.setItemAnimator(new DefaultItemAnimator());
		disasters = new ArrayList<>();
		adapter = new DisasterAdapter(activity, disasters);
		disasterList.setAdapter(adapter);
		getDisasters();
	}
	
	public void getDisasters() {
		disasters.clear();
		adapter.notifyDataSetChanged();
		activity.post(new BaseActivity.Listener() {

				@Override
				public void onResponse(String response) {
					try {
						JSONArray disastersJSON = new JSONArray(response);
						for (int i=0; i<disastersJSON.length(); i++) {
							JSONObject disasterJSON = disastersJSON.getJSONObject(i);
							disasters.add(disasterJSON);
						}
						adapter.notifyDataSetChanged();
					} catch (Exception e) {}
				}
		}, Constants.PHP_URL+"/main/get", "name", "disasters");
	}
}
