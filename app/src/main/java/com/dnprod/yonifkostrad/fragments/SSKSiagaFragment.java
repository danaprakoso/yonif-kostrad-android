package com.dnprod.yonifkostrad.fragments;

import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.view.View;
import com.dnprod.yonifkostrad.HomeActivity;
import com.dnprod.yonifkostrad.R;
import android.widget.ImageView;
import android.content.Intent;
import com.dnprod.yonifkostrad.ViewDocumentActivity;

public class SSKSiagaFragment extends Fragment {
	View view;
	HomeActivity activity;
	ImageView bateraiP, bateraiQ, bateraiR, bateraiMA;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// TODO: Implement this method
		view = inflater.inflate(R.layout.fragment_ssk_siaga, container, false);
		bateraiP = view.findViewById(R.id.baterai_p);
		bateraiQ = view.findViewById(R.id.baterai_q);
		bateraiR = view.findViewById(R.id.baterai_r);
		bateraiMA = view.findViewById(R.id.baterai_ma);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO: Implement this method
		super.onActivityCreated(savedInstanceState);
		activity = (HomeActivity)getActivity();
		bateraiP.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent i = new Intent(activity, ViewDocumentActivity.class);
					i.putExtra("title", "SSK Siaga");
					i.putExtra("url", "https://docs.google.com/spreadsheets/d/1vZlU4mXe-AgeQz339k4Qba5EFjsrzTVhcUkpmPWbTsg/edit#gid=2072466094");
					startActivity(i);
				}
		});
		bateraiQ.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent i = new Intent(activity, ViewDocumentActivity.class);
					i.putExtra("title", "SSK Siaga");
					i.putExtra("url", "https://docs.google.com/spreadsheets/d/1vZlU4mXe-AgeQz339k4Qba5EFjsrzTVhcUkpmPWbTsg/edit#gid=2072466094");
					startActivity(i);
				}
			});
		bateraiR.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent i = new Intent(activity, ViewDocumentActivity.class);
					i.putExtra("title", "SSK Siaga");
					i.putExtra("url", "https://docs.google.com/spreadsheets/d/1vZlU4mXe-AgeQz339k4Qba5EFjsrzTVhcUkpmPWbTsg/edit#gid=2072466094");
					startActivity(i);
				}
			});
		bateraiMA.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent i = new Intent(activity, ViewDocumentActivity.class);
					i.putExtra("title", "SSK Siaga");
					i.putExtra("url", "https://docs.google.com/spreadsheets/d/1vZlU4mXe-AgeQz339k4Qba5EFjsrzTVhcUkpmPWbTsg/edit#gid=2072466094");
					startActivity(i);
				}
			});
	}
}
