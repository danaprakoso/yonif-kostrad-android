package com.dnprod.yonifkostrad.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.dnprod.yonifkostrad.BaseActivity;
import com.dnprod.yonifkostrad.ChatActivity;
import com.dnprod.yonifkostrad.Constants;
import com.dnprod.yonifkostrad.HomeActivity;
import com.dnprod.yonifkostrad.NewsActivity;
import com.dnprod.yonifkostrad.R;
import com.dnprod.yonifkostrad.Util;
import com.dnprod.yonifkostrad.ViewDocumentActivity;
import com.dnprod.yonifkostrad.WebViewActivity;
import com.dnprod.yonifkostrad.AlertCommanderActivity;
import com.dnprod.yonifkostrad.AlertSoldierActivity;
import com.dnprod.yonifkostrad.SignupActivity;
import android.app.Activity;

import org.json.JSONArray;
import org.json.JSONObject;

public class HomeFragment extends Fragment {
	private final int SIGN_UP = 1;
	View view;
	HomeActivity activity;
	LinearLayout menu1, menu2, menu3, menu4, menu5, menu6, menu7, menu8, menu9, menu10;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// TODO: Implement this method
		view = inflater.inflate(R.layout.fragment_home, container, false);
		menu1 = view.findViewById(R.id.menu_1);
		menu2 = view.findViewById(R.id.menu_2);
		menu3 = view.findViewById(R.id.menu_3);
		menu4 = view.findViewById(R.id.menu_4);
		menu5 = view.findViewById(R.id.menu_5);
		menu6 = view.findViewById(R.id.menu_6);
		menu7 = view.findViewById(R.id.menu_7);
		menu8 = view.findViewById(R.id.menu_8);
		menu9 = view.findViewById(R.id.menu_9);
		menu10 = view.findViewById(R.id.menu_10);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO: Implement this method
		super.onActivityCreated(savedInstanceState);
		activity = (HomeActivity)getActivity();
		menu1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent i = new Intent(activity, WebViewActivity.class);
					i.putExtra("title", "Profil");
					i.putExtra("url", "https://www.instagram.com/yonarmed12kostrad");
					startActivity(i);
				}
		});
		menu2.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent i = new Intent(activity, ViewDocumentActivity.class);
					i.putExtra("title", "SSK Siaga");
					i.putExtra("url", "http://docs.google.com/spreadsheets/d/1pBBNSm7QlqqzeyxEZJ4D0JypFdu_nCUSeFaxFupVqsk/edit#gid=0");
					startActivity(i);
				}
			});
		menu3.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent i = new Intent(activity, WebViewActivity.class);
					i.putExtra("title", "Nominatif Batalyon");
					i.putExtra("url", "http://docs.google.com/spreadsheets/d/1kRUH9hwFV9Fr9uoPnSxrOtGXbMWTEX3I/edit?usp=drive_web&ouid=107435193818545314033&dls=true");
					startActivity(i);
				}
			});
		menu4.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					startActivity(new Intent(activity, ChatActivity.class));
				}
			});
		menu5.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent i = new Intent(activity, WebViewActivity.class);
					i.putExtra("title", "Pengaduan");
					i.putExtra("url", "https://drive.google.com/drive/u/0/my-drive");
					startActivity(i);
				}
			});
		menu6.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent i = new Intent(activity, WebViewActivity.class);
					i.putExtra("title", "Perlengkapan");
					i.putExtra("url", "http://docs.google.com/spreadsheets/d/1huKBSd4II_QobIthyCfXdDaaehrl7xHwv73I3WC2XBo/edit#gid=1286674887");
					startActivity(i);
				}
			});
		menu7.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent i = new Intent(activity, WebViewActivity.class);
					i.putExtra("title", "Instansi Lain");
					i.putExtra("url", "http://www.polri.go.id/");
					startActivity(i);
				}
			});
		menu8.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					/*if (!Util.isPackageInstalled(activity.getPackageManager(), "com.google.earth")) {
						AlertDialog dialog = new AlertDialog.Builder(activity)
							.setMessage("Untuk melihat halaman ini, Anda harus memasang aplikasi Google Earth. Apakah Anda ingin memasangnya sekarang?")
							.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface p1, int p2) {
									try {
										startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.earth")));
									} catch (android.content.ActivityNotFoundException anfe) {
										startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.google.earth")));
									}
								}
							})
							.setNegativeButton("Tidak", null)
							.create();
						dialog.show();
						return;
					}*/
					/*Intent i = new Intent(activity, WebViewActivity.class);
					i.putExtra("title", "Lokasi");
					i.putExtra("url", "http://earth.google.com/web/@-7.41241753,111.42323605,51.09423782a,1044.61319791d,35y,-178.89676625h,44.99998688t,0r/data=CpUBGpIBEosBCiUweDJlNzllNjJhNTMxNjI1ODE6MHg4YzZiNDhhZDc0OWU2OTg5GQnfoGJBph3AIc3_q44c21tAKlBSZXNpbWVuIEFydGlsZXJpIE1lZGFuIDEvIFB1dHJhIFl1ZGhhIEJhdGFseW9uIEFydGlsZXJpIE1lZGFuIDEyLyBBbmdpY2lwaSBZdWRoYRgCIAE");
					startActivity(i);*/
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.co.id/maps/place/Batalyon+Armed+12+Kostrad/@-7.4111206,111.4250673,1547m/data=!3m1!1e3!4m5!3m4!1s0x2e79e62b08f80403:0x98cae006a0e00e57!8m2!3d-7.4123275!4d111.4247963"));
					startActivity(browserIntent);
				}
			});
		menu9.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent i = new Intent(activity, NewsActivity.class);
					startActivity(i);
				}
			});
		menu10.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					final ProgressDialog dialog = Util.createDialog(activity, R.string.loading);
					dialog.show();
					activity.post(new BaseActivity.Listener() {

						@Override
						public void onResponse(String response) {
							try {
								dialog.dismiss();
								JSONObject obj = new JSONArray(response).getJSONObject(0);
								int profileCompleted = Util.getInt(obj, "profile_completed", 0);
								if (profileCompleted == 0) {
									startActivityForResult(new Intent(activity, SignupActivity.class), SIGN_UP);
								} else if (profileCompleted == 1) {
									startAlarmIntent();
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}, Constants.PHP_URL+"/main/get_by_id", "name", "users", "id", ""+Constants.USER_ID);
				}
			});
	}
	
	public void startAlarmIntent() {
		int role = Constants.ROLE;
		if (role == 0) {
			Intent i = new Intent(activity, AlertCommanderActivity.class);
			startActivity(i);
		} else if (role == 1) {
			Intent i = new Intent(activity, AlertSoldierActivity.class);
			startActivity(i);
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == SIGN_UP) {
				startAlarmIntent();
			}
		}
	}
}
