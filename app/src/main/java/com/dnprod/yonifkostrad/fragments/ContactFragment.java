package com.dnprod.yonifkostrad.fragments;

import androidx.fragment.app.Fragment;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import com.dnprod.yonifkostrad.HomeActivity;
import com.dnprod.yonifkostrad.R;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import org.json.JSONObject;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import com.dnprod.yonifkostrad.adapter.ContactAdapter;
import com.dnprod.yonifkostrad.BaseActivity;
import com.dnprod.yonifkostrad.Constants;
import org.json.JSONArray;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.content.Intent;
import com.dnprod.yonifkostrad.AddContactActivity;
import android.app.Activity;

public class ContactFragment extends Fragment {
	private final int ADD_CONTACT = 1;
	View view;
	HomeActivity activity;
	RecyclerView contactList;
	ArrayList<JSONObject> contacts;
	ContactAdapter adapter;
	int start = 0;
	int length = 20;
	boolean loading = false;
	FloatingActionButton add;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// TODO: Implement this method
		view = inflater.inflate(R.layout.fragment_contacts, container, false);
		contactList = view.findViewById(R.id.contacts);
		add = view.findViewById(R.id.add);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO: Implement this method
		super.onActivityCreated(savedInstanceState);
		activity = (HomeActivity)getActivity();
		contactList.setLayoutManager(new LinearLayoutManager(activity));
		contactList.setItemAnimator(new DefaultItemAnimator());
		contacts = new ArrayList<>();
		adapter = new ContactAdapter(activity, contacts);
		contactList.setAdapter(adapter);
		contactList.addOnScrollListener(new RecyclerView.OnScrollListener() {
			
				@Override
				public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
					super.onScrollStateChanged(recyclerView, newState);
					if (!recyclerView.canScrollVertically(1)) {
						if (contacts.size() > length) {
							getContacts(false);
						}
					}
				}
			});
		getContacts(true);
		add.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					startActivityForResult(new Intent(activity, AddContactActivity.class), ADD_CONTACT);
				}
				
		});
	}
	
	public void getContacts(final boolean reset) {
		if (loading) return;
		loading = true;
		if (reset) {
			start = 0;
			contacts.clear();
			adapter.notifyDataSetChanged();
		}
		activity.post(new BaseActivity.Listener() {

				@Override
				public void onResponse(String response) {
					try {
						JSONArray contactsJSON = new JSONArray(response);
						for (int i=0; i<contactsJSON.length(); i++) {
							JSONObject contactJSON = contactsJSON.getJSONObject(i);
							contacts.add(contactJSON);
						}
						adapter.notifyDataSetChanged();
						start += length;
						loading = false;
					} catch (Exception e) {}
				}
		}, Constants.PHP_URL+"/user/get_contacts", "start", ""+start, "length", ""+length);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO: Implement this method
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == ADD_CONTACT) {
				getContacts(true);
			}
		}
	}
}
