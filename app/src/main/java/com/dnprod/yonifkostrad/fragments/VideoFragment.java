package com.dnprod.yonifkostrad.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import com.dnprod.yonifkostrad.BaseActivity;
import com.dnprod.yonifkostrad.Constants;
import com.dnprod.yonifkostrad.HomeActivity;
import com.dnprod.yonifkostrad.R;
import com.dnprod.yonifkostrad.Util;
import com.dnprod.yonifkostrad.adapter.VideoAdapter;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dnprod.yonifkostrad.PathUtil;
import android.media.ThumbnailUtils;
import android.graphics.Bitmap;
import java.io.FileOutputStream;
import com.dnprod.yonifkostrad.SignupActivity;

public class VideoFragment extends Fragment
{
	private final int CAPTURE_VIDEO = 1;
	private final int PICK_VIDEO_FROM_GALLERY = 2;
	private final int SIGN_UP = 3;
	View view;
	HomeActivity activity;
	RecyclerView videoList;
	ArrayList<JSONObject> videos;
	VideoAdapter adapter;
	int start = 0;
	int length = 20;
	FloatingActionButton add, filter;
	SwipeRefreshLayout swipe;
	ProgressBar progress;
	CardView uploadContainer;
	int currentCategory = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// TODO: Implement this method
		view = inflater.inflate(R.layout.fragment_videos, container, false);
		videoList = view.findViewById(R.id.videos);
		add = view.findViewById(R.id.add);
        filter = view.findViewById(R.id.filter);
        swipe = view.findViewById(R.id.swipe);
		progress = view.findViewById(R.id.progress);
		uploadContainer = view.findViewById(R.id.upload_container);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO: Implement this method
		super.onActivityCreated(savedInstanceState);
		activity = (HomeActivity)getActivity();
		videoList.setLayoutManager(new LinearLayoutManager(activity));
		videoList.setItemAnimator(new DefaultItemAnimator());
		videos = new ArrayList<>();
		adapter = new VideoAdapter(activity, videos);
		videoList.setAdapter(adapter);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                getVideos(currentCategory);
            }
        });
		getLatestVideos(true);
		add.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view)
				{
					int userID = Constants.USER_ID;
					if (userID == 0) {
						startActivityForResult(new Intent(activity, SignupActivity.class), SIGN_UP);
					} else {
						selectVideo();
					}
				}
			});
        filter.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                AlertDialog dialog = new AlertDialog.Builder(activity)
                        .setSingleChoiceItems(new String[] {
                                "Latest", "Best Rated", "Most Viewed",
                                "Featured", "Near You", "Favorites", "Your Videos"
                        }, 0, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface p1, int position)
                            {
                                currentCategory = position;
                                getVideos(position);
                            }
                        })
                        .setTitle("Urutkan Berdasarkan")
                        .create();
                dialog.show();
            }
        });
	}
	
	public void selectVideo() {
		AlertDialog dialog = new AlertDialog.Builder(activity)
			.setItems(new String[] {
				"Kamera", "Galeri"
			}, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface p1, int position)
				{
					if (position == 0)
					{
						Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
						if (takeVideoIntent.resolveActivity(activity.getPackageManager()) != null)
						{
							startActivityForResult(takeVideoIntent, CAPTURE_VIDEO);
						}
					}
					else if (position == 1)
					{
						Intent intent = new Intent();
						intent.setType("video/*");
						intent.setAction(Intent.ACTION_GET_CONTENT);
						startActivityForResult(Intent.createChooser(intent, "Select Video"), PICK_VIDEO_FROM_GALLERY);
					}
				}
			})
			.create();
		dialog.show();
	}

	public void getLatestVideos(final boolean reset)
	{
		if (reset)
		{
			videos.clear();
			adapter.notifyDataSetChanged();
			start = 0;
		}
		activity.post(new BaseActivity.Listener() {

				@Override
				public void onResponse(String response)
				{
					try
					{
						JSONArray videosJSON = new JSONArray(response);
						for (int i=0; i < videosJSON.length(); i++)
						{
							videos.add(videosJSON.getJSONObject(i));
						}
						adapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);
					}
					catch (Exception e)
					{}
				}
			}, Constants.PHP_URL + "/user/get_latest_videos", "start", "" + start, "length", "" + length);
	}

	public void getBestRatedVideos(final boolean reset)
	{
		if (reset)
		{
			videos.clear();
			adapter.notifyDataSetChanged();
			start = 0;
		}
		activity.post(new BaseActivity.Listener() {

				@Override
				public void onResponse(String response)
				{
					try
					{
						JSONArray videosJSON = new JSONArray(response);
						for (int i=0; i < videosJSON.length(); i++)
						{
							videos.add(videosJSON.getJSONObject(i));
						}
						adapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);
					}
					catch (Exception e)
					{}
				}
			}, Constants.PHP_URL + "/user/get_best_rated_videos", "start", "" + start, "length", "" + length);
	}

	public void getMostViewedVideos(final boolean reset)
	{
		if (reset)
		{
			videos.clear();
			adapter.notifyDataSetChanged();
			start = 0;
		}
		activity.post(new BaseActivity.Listener() {

				@Override
				public void onResponse(String response)
				{
					try
					{
						JSONArray videosJSON = new JSONArray(response);
						for (int i=0; i < videosJSON.length(); i++)
						{
							videos.add(videosJSON.getJSONObject(i));
						}
						adapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);
					}
					catch (Exception e)
					{}
				}
			}, Constants.PHP_URL + "/user/get_most_viewed_videos", "start", "" + start, "length", "" + length);
	}

	public void getFeaturedVideos(final boolean reset)
	{
		if (reset)
		{
			videos.clear();
			adapter.notifyDataSetChanged();
			start = 0;
		}
		activity.post(new BaseActivity.Listener() {

				@Override
				public void onResponse(String response)
				{
					try
					{
						JSONArray videosJSON = new JSONArray(response);
						for (int i=0; i < videosJSON.length(); i++)
						{
							videos.add(videosJSON.getJSONObject(i));
						}
						adapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);
					}
					catch (Exception e)
					{}
				}
			}, Constants.PHP_URL + "/user/get_featured_videos", "start", "" + start, "length", "" + length);
	}

	public void getNearestVideos(final boolean reset)
	{
		if (reset)
		{
			videos.clear();
			adapter.notifyDataSetChanged();
			start = 0;
		}
		activity.getLocation(new BaseActivity.LocationUpdateListener() {

				@Override
				public void onLocationUpdate(double lat, double lng)
				{
					activity.post(new BaseActivity.Listener() {

							@Override
							public void onResponse(String response)
							{
								try
								{
									JSONArray videosJSON = new JSONArray(response);
									for (int i=0; i < videosJSON.length(); i++)
									{
										videos.add(videosJSON.getJSONObject(i));
									}
									adapter.notifyDataSetChanged();
                                    swipe.setRefreshing(false);
								}
								catch (Exception e)
								{}
							}
						}, Constants.PHP_URL + "/user/get_nearest_videos", "lat", "" + lat, "lng", "" + lng, "start", "" + start, "length", "" + length);
				}
			});
	}

	public void getFavoriteVideos(final boolean reset)
	{
		if (reset)
		{
			videos.clear();
			adapter.notifyDataSetChanged();
			start = 0;
		}
		activity.post(new BaseActivity.Listener() {

				@Override
				public void onResponse(String response)
				{
					try
					{
						JSONArray videosJSON = new JSONArray(response);
						for (int i=0; i < videosJSON.length(); i++)
						{
							videos.add(videosJSON.getJSONObject(i));
						}
						adapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);
					}
					catch (Exception e)
					{}
				}
			}, Constants.PHP_URL + "/user/get_favorite_videos", "user_id", "" + Constants.USER_ID, "start", "" + start, "length", "" + length);
	}

	public void getMyVideos(final boolean reset)
	{
		if (reset)
		{
			videos.clear();
			adapter.notifyDataSetChanged();
			start = 0;
		}
		activity.post(new BaseActivity.Listener() {

				@Override
				public void onResponse(String response)
				{
					try
					{
						JSONArray videosJSON = new JSONArray(response);
						for (int i=0; i < videosJSON.length(); i++)
						{
							videos.add(videosJSON.getJSONObject(i));
						}
						adapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);
					}
					catch (Exception e)
					{}
				}
			}, Constants.PHP_URL + "/user/get_my_videos", "user_id", "" + Constants.USER_ID, "start", "" + start, "length", "" + length);
	}
	
	public void getVideos(int position) {
		if (position == 0)
		{
			getLatestVideos(true);
		}
		else if (position == 1)
		{
			getBestRatedVideos(true);
		}
		else if (position == 2)
		{
			getMostViewedVideos(true);
		}
		else if (position == 3)
		{
			getFeaturedVideos(true);
		}
		else if (position == 4)
		{
			getNearestVideos(true);
		}
		else if (position == 5)
		{
			getFavoriteVideos(true);
		}
		else if (position == 6)
		{
			getMyVideos(true);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO: Implement this method
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK)
		{
			if (requestCode == CAPTURE_VIDEO)
			{
				uploadVideo(data.getData());
			}
			else if (requestCode == PICK_VIDEO_FROM_GALLERY)
			{
				uploadVideo(data.getData());
			} else if (requestCode == SIGN_UP) {
				selectVideo();
			}
		}
	}

	public void uploadVideo(final Uri uri) {
	    View view = LayoutInflater.from(activity).inflate(R.layout.enter_video_title, null);
	    final EditText videoTitleField = view.findViewById(R.id.video_title);
	    final AlertDialog dialog = new AlertDialog.Builder(activity)
                .setView(view)
                .setPositiveButton("OK", null)
                .setNegativeButton(R.string.cancel, null)
                .setCancelable(false)
                .create();
	    dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialogInterface) {
                dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String videoTitle = videoTitleField.getText().toString().trim();
                        if (videoTitle.equals("")) {
                            activity.show("Mohon masukkan judul video");
                            return;
                        }
                        dialog.dismiss();
                        final ProgressDialog dialog = activity.createDialog("Mengunggah video...");
                        dialog.show();
                        uploadContainer.setVisibility(View.VISIBLE);
                        final String path = PathUtil.getPath(activity, uri);
                        final File videoFile = new File(path);
                        Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(videoFile.getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                        final File thumbnailFile = new File(activity.getFilesDir(), UUID.randomUUID().toString()+".jpg");
                        try {
                            FileOutputStream fos = new FileOutputStream(thumbnailFile);
                            thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                            fos.flush();
                            fos.close();
                        } catch (Exception e) {}
                        activity.getLocation(new BaseActivity.LocationUpdateListener() {

                            @Override
                            public void onLocationUpdate(final double lat, final double lng) {
                                activity.run(new Runnable() {

                                    @Override
                                    public void run()
                                    {
                                        progress.setMax((int)videoFile.length());
                                        progress.setProgress(0);
                                        try
                                        {
                                            OkHttpClient client = Util.getOkHttpClient();
                                            RequestBody params = new MultipartBuilder()
                                                    .type(MultipartBuilder.FORM)
                                                    .addFormDataPart("uploader_id", ""+Constants.USER_ID)
                                                    .addFormDataPart("date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()))
                                                    .addFormDataPart("lat", ""+lat)
                                                    .addFormDataPart("lng", ""+lng)
                                                    .addFormDataPart("title", videoTitle)
                                                    .addFormDataPart("video", UUID.randomUUID().toString() + Util.getExtension(path), new ProgressRequestBody(videoFile, Util.getMediaType(activity, uri), new ProgressListener() {

                                                        @Override
                                                        public void transferred(final long num)
                                                        {
                                                            // TODO: Implement this method
                                                            activity.runLater(new Runnable() {

                                                                @Override
                                                                public void run()
                                                                {
                                                                    // TODO: Implement this method
                                                                    progress.setProgress((int)num);
                                                                }
                                                            });
                                                        }

                                                    }))
                                                    .addFormDataPart("screenshot", UUID.randomUUID().toString()+".jpg", RequestBody.create(MediaType.parse("image/jpeg"), thumbnailFile))
                                                    .build();
                                            Request request = new Request.Builder()
                                                    .url(Constants.PHP_URL + "/user/upload_video")
                                                    .post(params)
                                                    .build();
                                            final String response = client.newCall(request).execute().body().string();
                                            activity.runLater(new Runnable() {

                                                @Override
                                                public void run()
                                                {
                                                    // TODO: Implement this method
                                                    uploadContainer.setVisibility(View.GONE);
                                                    dialog.dismiss();
                                                    //activity.show("Video berhasil diunggah");
                                                    getVideos(currentCategory);
                                                }
                                            });
                                        }
                                        catch (final Exception e)
                                        {
                                            activity.runLater(new Runnable() {

                                                @Override
                                                public void run()
                                                {
                                                    // TODO: Implement this method
                                                    activity.show(e.getMessage());
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
	    dialog.show();
	}

	public class ProgressRequestBody extends RequestBody
	{
		private static final int SEGMENT_SIZE = 2048; // okio.Segment.SIZE

		private final File file;
		private final ProgressListener listener;
		private final String contentType;

		public ProgressRequestBody(File file, String contentType, ProgressListener listener)
		{
			this.file = file;
			this.contentType = contentType;
			this.listener = listener;
		}

		@Override
		public long contentLength()
		{
			return file.length();
		}

		@Override
		public MediaType contentType()
		{
			return MediaType.parse(contentType);
		}

		@Override
		public void writeTo(BufferedSink sink) throws IOException
		{
			Source source = null;
			try
			{
				source = Okio.source(file);
				long total = 0;
				long read;

				while ((read = source.read(sink.buffer(), SEGMENT_SIZE)) != -1)
				{
					total += read;
					sink.flush();
					this.listener.transferred(total);

				}
			}
			finally
			{
				com.squareup.okhttp.internal.Util.closeQuietly(source);
			}
		}
	}

	public interface ProgressListener
	{
		void transferred(long num);
	}
}
