package com.dnprod.yonifkostrad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.content.ContextCompat;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Util.show(context, "AUTO START ON BOOT");
        Intent i = new Intent(context, BackgroundService.class);
        if (!BackgroundService.started) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ContextCompat.startForegroundService(context, i);
            } else {
                context.startService(i);
            }
        }
    }
}
