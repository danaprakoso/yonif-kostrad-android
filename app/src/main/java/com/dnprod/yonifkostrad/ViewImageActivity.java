package com.dnprod.yonifkostrad;

import android.os.Bundle;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import android.net.Uri;

public class ViewImageActivity extends BaseActivity {
	ImageView img;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_image);
		img = findViewById(R.id.img);
		String imageURL = getIntent().getStringExtra("img_url");
		Picasso.get().load(Uri.parse(imageURL)).resize(2048, 0).onlyScaleDown().into(img);
	}
}
