package com.dnprod.yonifkostrad.newsfragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.dnprod.yonifkostrad.R;
import android.webkit.WebViewClient;
import android.graphics.Bitmap;
import android.webkit.WebChromeClient;
import com.dnprod.yonifkostrad.NewsActivity;

public class NewsFragment extends Fragment {
	View view;
	NewsActivity activity;
	WebView web;
	ProgressBar progress;
	SwipeRefreshLayout swipe;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// TODO: Implement this method
		view = inflater.inflate(R.layout.fragment_news, container, false);
		web = view.findViewById(R.id.web);
		progress = view.findViewById(R.id.progress);
		swipe = view.findViewById(R.id.swipe);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO: Implement this method
		super.onActivityCreated(savedInstanceState);
		activity = (NewsActivity)getActivity();
		web.setWebViewClient(new WebViewClient() {
			
			@Override
			public void onPageStarted(WebView web, String url, Bitmap favicon) {
				progress.setProgress(10);
				progress.setVisibility(View.VISIBLE);
			}
			
			@Override
			public void onPageFinished(WebView web, String url) {
				progress.setVisibility(View.GONE);
				swipe.setRefreshing(false);
			}
		});
		web.setWebChromeClient(new WebChromeClient() {
			
			@Override
			public void onProgressChanged(WebView web, int newProgeess) {
				if (newProgeess < 10) {
					progress.setProgress(10);
				} else {
					progress.setProgress(newProgeess);
				}
			}
		});
		web.getSettings().setJavaScriptEnabled(true);
		web.getSettings().setUseWideViewPort(true);
		web.loadUrl(getArguments().getString("url"));
		swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

				@Override
				public void onRefresh() {
					web.reload();
				}
		});
	}
}
